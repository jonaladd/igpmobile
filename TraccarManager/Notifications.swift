//
//  Notifications.swift
//  com.igpsmobile
//
//  Created by Jonathan Leon on 19-06-18.
//  Copyright © 2018 i-gps.com. All rights reserved.
//

import UIKit
import Alamofire
class Notifications: UITableViewController {
    static let sharedInstance4 = RutasDevices()
    var dispositivo2 = Device()
    struct User {
        var name: String
        var device: String
       
    }
     var util: [User]?
    var dispo: String?
    var devices: [Device]?
    var devices2: [Device]?
    var devices4: [Device]?
     var devices3 = [String]()
    @IBOutlet weak var searchBar: UISearchBar!
    var filteredData: [String]!
    var filteredPersons : [Device]?
    var filteredExercises = [String]()
    var isFiltering = false
    // nuevo
    var data = [String]()
    var searchActive = true
    var account: String?
    var name: String?
    var direccion : String?
   var contador  = 0
    var email: String?
    var filteredArray = [String]()
    var filtered:[String] = []
    
    var shouldShowSearchResults = false
    var datosFilter = [String]()
    var barraActiva = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        actualizar()
         tableView.dataSource = self
        fetchDevices2()
        
        UIApplication.shared.applicationIconBadgeNumber = 0
       
        
        // update the list when we're told that a Position has been updated (this is the "Updated ... ago" message)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(RutasDevices.reloadDevices),
                                               name: NSNotification.Name(rawValue: Definitions.PositionUpdateNotificationName),
                                               object: nil)
        
        // update the list when a Device has been updated (maybe the name has changed, or an addition/removal of a device)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(RutasDevices.reloadDevices),
                                               name: NSNotification.Name(rawValue: Definitions.DeviceUpdateNotificationName),
                                               object: nil)
        
        if Definitions.isRunningOniPad {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                                     target: self,
                                                                     action: #selector(RutasDevices.close))
        }
    }
   
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    func actualizar() {
         devices = WebService.sharedInstance.devices
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        reloadDevices()
    }
    
    @objc func reloadDevices() {
        //devices = WebService.sharedInstance.devices
        tableView.reloadData()
    }
    
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            //return isFiltering == true ? filteredPersons!.count : d.count
            return !barraActiva ? data.count : datosFilter.count
            
            
        
        
        return 0
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    

    
   
    
    func fetchDevices2(_ onFailure: ((String) -> Void)? = nil)  {
         let dd = Position()
        let account = WebService.sharedInstance.account
        let email = WebService.sharedInstance.email
        let password = WebService.sharedInstance.password
        let  serverURL  = "http://site1.i-gps.cl/CDCmobile/android/"
       // let  serverURL  = "http://186.10.30.106:82/CDCmobile/ios/"
        
        let url = serverURL + "api/alarmas/?accountID=" + account!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&userID=" + email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&password=" + password!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        Alamofire.request(url).responseJSON(completionHandler: { response in
            switch response.result {
                
                
            case .success(let JSON):
                if response.response!.statusCode != 200 {
                    if let fail = onFailure {
                        fail("Invalid server response")
                    }
                } else {
                    
                    if let data = JSON as? [[String : AnyObject]] {
                        if(data.isEmpty)
                        {
                            let ac = UIAlertController(title: "INFORMACIÓN", message: "No hay alarmas disponibles para las últimas 24 horas", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                            ac.addAction(okAction)
                            self.present(ac, animated: true, completion: nil)
                        }
                         var positions = [Position]()
                       
                        
                        for d in data {
                            
                            let dd = Position()
                            
                            
                            
                            dd.setValuesForKeys(d)
                            positions.append(dd)
                           
                            
                           
                          
                            
                            for d in self.devices! {
                            
                                if(d.deviceId == dd.deviceId2){
                                
                               
                                self.name = d.name
                                }
                                else{
                                    //self.name = "no hay info"
                                }
                                
                            }
                        
                   
                            if(dd.alarmaString == "En Reposo")
                            {
                                
                                if(self.name != nil){
                                
                                var title = "En Reposo:  el vehículo " + self.name! + "  está en reposo en el \t" + dd.fixTime!
                                
                                
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Exceso de Reposo")
                            {
                                  if(self.name != nil){
                                var title = "Exceso de Reposo:  el vehículo " + self.name! + " excedio el tiempo limite de reposo en el  en el \t" + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Entrada en Geozona")
                            {
                                  if(self.name != nil){
                                var title = "Entrada en Geozona: el vehículo " + self.name! + " entro en la geozona en el \t" + dd.geozoneID! + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Motor Encendido")
                            {
                                  if(self.name != nil){
                                var title = "Motor Encendido: el vehículo "  + self.name! + " encendio el motor  en el " + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
 
                            if(dd.alarmaString == "Motor Apagado")
                            {
                                  if(self.name != nil){
                                var title = "Motor Apagado: el vehículo " +  self.name! + " apago el motor en el " + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            
                            
                            if(dd.alarmaString == "Salida de Geozona")
                            {
                                  if(self.name != nil){
                                var title = "Salida de Geozona: el vehículo " +  self.name! + " Salio de la geozona \t" + dd.geozoneID! + " en el " + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Transito en Geozona")
                            {
                                  if(self.name != nil){
                                var title = "Transito en Geozona: el vehículo " + self.name! + " Transita en ale geozona  \t" + dd.geozoneID! + " en el " + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Tiempo en Geozonas")
                            {
                                  if(self.name != nil){
                                
                    var title = "Tiempo en Geozonas: el vehículo" + self.name! + " Excedio el tiempo en la geozona \t" + dd.geozoneID! + " en el " + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Boton de Panico")
                            {
                                  if(self.name != nil){
                                
                                var title = "Boton de Panico: el vehículo " + self.name! + " presiono el botón de pánico en el \t" + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Apertura de Puertas")
                            {
                                  if(self.name != nil){
                                var title = "Apertura de Puertas: el vehículo " + self.name! + " abrio las puertas en el \t" + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Cierre de Puertas")
                            {
                                  if(self.name != nil){
                                var title = "Cierre de Puertas: el vehículo " + self.name! + " cerro las puerta en el \t" + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Falla de Energia")
                            {
                                  if(self.name != nil){
                                var title = "Falla de Energía: el vehículo " + self.name! + " sefrio un falla de energía en el  \t" + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Velocidad")
                            {
                                  if(self.name != nil){
                                var title = "Exceso de velocidad: el vehículo " + self.name! + " excedio la velocidad maxima en el \t" + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                            if(dd.alarmaString == "Velocidad maxima en Geozona")
                            {
                                  if(self.name != nil){
                                var title = "Exceso de velocidad en geozona: el vehículo " + self.name! + " excedio la velocida maxima en la geozona \t" + dd.geozoneID! + "en el" + dd.fixTime!
                                self.devices3.append(title)
                                self.data.append(title)
                                }
                            }
                           
                          
                            
                           
                            
                            
                        }
                        
                        
                    } else {
                        if let fail = onFailure {
                            fail("Server response was invalid")
                        }
                    }
                }
                
            case .failure(let error):
               let ac = UIAlertController(title: "No Hay conexion con el servidor ", message: "verifique su conexión o  si sus datos de sesión fueron actualizados", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                ac.addAction(okAction)
                self.present(ac, animated: true, completion: nil)
                if let fail = onFailure {
                    fail(error.localizedDescription)
                }
            }
        })
    }
    
    
    // esta funcion realiza el filtro de la lista
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            datosFilter = data.filter { $0.lowercased().contains(searchText.lowercased()) }
            barraActiva = true
        } else {
            datosFilter.removeAll(keepingCapacity: true)
            barraActiva = false
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        datosFilter.removeAll(keepingCapacity: true)
        barraActiva = false
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        
      
            if !barraActiva {
                
                
               // print(data)
                cell.textLabel!.text = devices3[indexPath.row]
                cell.textLabel?.numberOfLines = 0
                cell.detailTextLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                cell.detailTextLabel?.isHidden = true
                
                
               
                
            } else {
                cell.textLabel?.text = datosFilter[indexPath.row]
                cell.textLabel?.numberOfLines = 0
                cell.detailTextLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                cell.detailTextLabel?.isHidden = true
                
                
            }
            
            
        
        
        
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dvc = segue.destination as? DeviceInfoViewController {
            let idxp = tableView.indexPathForSelectedRow
            
            
            
            if let d = devices {
                
                if !barraActiva {
                    
                    /* datos sin filtro */
                    for d in devices! {
                       
                            var string = devices3[(idxp! as NSIndexPath).row]
                            
                            if string.range(of:d.name!) != nil {
                                //print("existe1")
                                dvc.device = d
                            }
                            
                        
                        
                    }
                    

                    
                //    dvc.device = d[(idxp! as NSIndexPath).row]
                    
                  /* datos con filtros*/
                } else {
                    let string = datosFilter[(idxp! as NSIndexPath).row]
                    for d in devices! {
                        if string.range(of:d.name!) != nil {
                           // print("existe2")
                            dvc.device = d
                        }
                        
                    }
                    
                }
                
                
                
                
            }
            
            
            
            
        }
    }
    
    
}



