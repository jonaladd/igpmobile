//
//  RepeatingTimer.swift
//  com.igpsmobile
//
//  Created by Jonathan Leon on 20-06-18.
//  Copyright © 2018 i-gps.com. All rights reserved.
//

import Foundation
class RepeatingTimer {
    let timeInterval: TimeInterval
    init(timeInterval: TimeInterval) {
        self.timeInterval = timeInterval
    }
    private lazy var timer: DispatchSourceTimer = {
        let t = DispatchSource.makeTimerSource()
      //  t.schedule(deadline: .now() + self.timeInterval, repeating: self.timeInterval)
     // t.scheduleRepeating(deadline: .now() + self.timeInterval, interval: self.timeInterval)
        //t.scheduleRepeating(deadline: .now() + self.timeInterval, interval: self.timeInterval)
     t.scheduleRepeating(deadline: .now() + self.timeInterval, interval: self.timeInterval, leeway: .milliseconds(100))
        t.setEventHandler(handler: { [weak self] in
            self?.eventHandler?()
        })
        return t
    }()
    var eventHandler: (() -> Void)?
    private enum State {
        case suspended
        case resumed
    }
    private var state: State = .suspended
    func resume() {
        if state == .resumed {
            return
        }
        state = .resumed
        timer.resume()
    }
    
    func suspend() {
        if state == .suspended {
            return
        }
        state = .suspended
        timer.suspend()
    }
}
