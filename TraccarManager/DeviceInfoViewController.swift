/* Code developed by  Jonathan León  */

import UIKit
import MapKit


class DeviceInfoViewController: UITableViewController, MKMapViewDelegate {
    var color: UIColor?
    var device: Device?
    var filtro2: String?
    var filtro: NSNumber?
    let TCDefaultsAccountKey = "DefaultsAccountKey"
    let TCDefaultsEmailKey = "DefaultsEmailKey"
    let TCDefaultsPasswordKey = "DefaultsPasswordKey"
    let TCDefaultsAlertKey = "DefaultsAlertKey"
    let TCDefaultsMapsKey = "DefaultsMapsKey"
    var mapa: String?
    @IBOutlet var mapView: MKMapView?
    
    fileprivate var devices: [Device] = []
    
    fileprivate var positions: [Position] = []
    
    fileprivate var shouldCenterOnAppear: Bool = true
    
    // var selectedAnnotation: PositionAnnotation?
    var selectedAnnotation:  ColorPointAnnotation?
    static let sharedInstance3 = DeviceInfoViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView?.delegate = self
        let d = UserDefaults.standard
        if let e = d.string(forKey: TCDefaultsMapsKey ) {
            mapa = e
            
            
        }
        if(mapa != nil)
        {
            if(mapa == "MKMapType.standard"){
                mapView?.mapType = MKMapType.standard;
            }
            if(mapa == "MKMapType.satellite"){
                mapView?.mapType = MKMapType.satellite;
            }
            if(mapa == "MKMapType.hybrid"){
                mapView?.mapType = MKMapType.hybrid;
            }
        }
        else{
            mapView?.mapType = MKMapType.standard;
            let d = UserDefaults.standard
            
            d.setValue("MKMapType.standard", forKey: TCDefaultsMapsKey)
            // d.setValue("no", forKey: TCDefaultsAlertKey)
            d.synchronize()
        }
        mapView?.showsPointsOfInterest = true;
        mapView?.showsTraffic = true;
        mapView?.showsCompass = true;
        //mapView?.mapType = MKMapType.hybridFlyover;
        mapView?.isPitchEnabled = true
        mapView?.isRotateEnabled = true
        
        
        // no permita que el usuario abra dispositivos vista hasta que los dispositivos hayan sido cargados
        // self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    var shouldShowCloseButton: Bool = false
    fileprivate var deviceProperties: [String] = [
        "Name",
        "Conexión"
    ]
    fileprivate var positionProperties: [String] = [
        "Última Ubicación",
        "Dirección",
        "Velocidad",
        "geozona",
        "Latitud",
        "Longitud",
        "estado",
        "Altitud",
        "Curso",
        "Tiempo En Geozona"
    ]
    
    func close() {
      
        dismiss(animated: true, completion: nil)
        
    }

    override func viewWillAppear(_ animated: Bool) {
   
        super.viewWillAppear(animated)
        
        if let d = device {
            self.navigationItem.title = d.name
            
        }
        
        if shouldShowCloseButton {
    

            if Definitions.isRunningOniPad {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                                         target: self,
                                                                         action: #selector(DeviceInfoViewController.close))
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        

        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if section == 0 {
            return deviceProperties.count
        } else if section == 1 {
            return positionProperties.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
     
        if section == 0 {
            return "Dispositivo"
        } else if section == 1 {
            return "Posicion"
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //este matiene la actualizacion al tocar la pantalla
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        cell.detailTextLabel!.text = "–"
        
        if let d = device {
            
            if (indexPath as NSIndexPath).section == 0 {
                
                let property = deviceProperties[(indexPath as NSIndexPath).row]
                cell.textLabel!.text = property
                
                var keyPath = property.camelCasedString
             
                if property == "Conexión" {
                    keyPath = "statusString"
                }
                filtro = d.id
                let numberFormatter = NumberFormatter()
                filtro2 = numberFormatter.string(from: filtro!)! as String?
                if let value = d.value(forKey: keyPath) {
                    cell.detailTextLabel!.text = "\(value)"
                }
                
            } else if (indexPath as NSIndexPath).section == 1 {
                
                let property = positionProperties[(indexPath as NSIndexPath).row]
                cell.textLabel!.text = property

                            if let position = d.position {
                                
                                var keyPath = property.camelCasedString
                                if property == "Latitud" {
                                    keyPath = "latitudeString"
                                } else if property == "Longitud" {
                                    keyPath = "longitudeString"
                                    
                                }
                                else if property == "Dirección" {
                                    keyPath = "address"
                                }
                                else if property == "Curso" {
                                    keyPath = "courseDirectionString"
                                } else if property == "Velocidad" {
                                    keyPath = "speedString"
                                }else if property == "Última Ubicación" {
                                    keyPath = "fixTime"
                                }else if property == "geozona" {
                                    keyPath = "geozonaID"
                                }
                                else if property == "Tiempo En Geozona" {
                                    keyPath = "timestampGeozone"
                                }
                                else if property == "estado" {
                                    keyPath = "estadoString"
                                }
                                else if property == "Altitud" {
                                    keyPath = "altitude"
                                }
                                
                                
                                if let value = position.value(forKey: keyPath) {
                                    cell.detailTextLabel!.text = "\(value)"
                                }
                }
                
            }
            
        }
       // zoomToAllButtonPressed()
        return cell
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        zoomToAllButtonPressed()
        // necesitamos disparar esto manualmente cuando aparece la vista
        loginStatusChanged()
    }
    @IBAction func zoomToAllButtonPressed() {
        
            mapView?.removeAnnotations((mapView?.annotations)!)
            WebService.sharedInstance.reconnectWebSocket2()
            WebService.sharedInstance.fetchDevices(onSuccess: { (newDevices) in
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.devices = newDevices
            self.refreshPositions()
            //mapView?.showAnnotations((mapView?.annotations)!, animated: true)
            self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: false)
            
        })
        
            //mapView?.showAnnotations((mapView?.annotations)!, animated: true)
            self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: false)
    }
    
    func loginStatusChanged() {
        
        //zoomToAllButtonPressed()
        if User.sharedInstance.isAuthenticated {
            
            WebService.sharedInstance.fetchDevices(onSuccess: { (newDevices) in
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                self.devices = newDevices
                self.refreshPositions()
                self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: false)
                
            })
            
        } else {
            performSegue(withIdentifier: "ShowLogin", sender: self)
        }
        
  
    }
    
    func refreshPositions() {
    
        mapView?.removeAnnotations((mapView?.annotations)!)
    
        self.positions = WebService.sharedInstance.positions
        //  for device in self.devices {
        for device in self.devices {
            var annotationForDevice: ColorPointAnnotation?
            for existingAnnotation in (self.mapView?.annotations)! {
                if let a = existingAnnotation as? ColorPointAnnotation {
                    if a.deviceId == device.id {
                        //if a.deviceId == filtro {
                        annotationForDevice = a
                        break
                    }
                }
            }
            if let a = annotationForDevice {
                
                if let p = WebService.sharedInstance.positionByDeviceId(a.deviceId!) {
                    if p.id == a.positionId {
                        // this annotation is still current, don't change it
                    } else {
                            a.coordinate = p.coordinate
                            a.title = p.annotationTitle
                          //  a.subtitle = p.annotationSubtitle
                        a.subtitle = p.estadoString
                            a.color = .green
                            // a.positionId = p.id
                            // device ID will not have changed
                    }
                } else {
                    // the position for this device has been removed
                    if device.id != filtro {
                        self.mapView?.removeAnnotation(a)
                    }
                }
                
            } else {
            
                if let p = WebService.sharedInstance.positionByDeviceId(device.id!) {
                       // let a = PositionAnnotation()
                        let a = ColorPointAnnotation()
                        a.coordinate = p.coordinate
                        a.title = p.annotationTitle
                       // a.subtitle = p.annotationSubtitle
                        a.subtitle = p.estadoString
                       // a.positionId = p.id
                       // a.deviceId = p.deviceId
                        a.color = .green
                    
                    if device.id == filtro {
                        self.mapView?.addAnnotation(a)
                        self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: true)
                    }
                    
                }
                
            }
        }
 
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            pinView!.canShowCallout = true
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            let lbl = UILabel(frame: CGRect(x: -25, y: -10, width: 70, height: 20))
            lbl.textColor = .black
            lbl.tag = 100
            lbl.adjustsFontSizeToFitWidth = true
            lbl.backgroundColor = self.color
            pinView?.addSubview(lbl)
            pinView?.canShowCallout = true
            pinView?.isSelected = true
            pinView?.isHighlighted = true
        }
       
                        let lbl = pinView?.viewWithTag(100) as! UILabel
        
                        if(annotation.subtitle! == "En Movimiento")
                        {
                            
                            lbl.backgroundColor = .green
                        }
                        else if(annotation.subtitle! == "Detenido")
                            
                        {
                          lbl.backgroundColor = .red
                            //  lbl.backgroundColor = .green
                        }
                        else if(annotation.subtitle! == "Entrada A Geozona" || annotation.subtitle! == "Salida De Geozona" )
                            
                        {
                            lbl.backgroundColor = .orange
                            
                        }
                        else{
                            lbl.backgroundColor = .red
                        }
        
        lbl.text = annotation.title!
        pinView!.annotation = annotation
        return pinView
    }
    

}

