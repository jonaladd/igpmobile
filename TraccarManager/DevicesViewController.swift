
// Create by Jonathan León Yañez
import UIKit
import Alamofire
class DevicesViewController: UITableViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    var filteredData: [String]!
    var devices: [Device]?
    var devices2: [Device]?
    var filteredPersons : [Device]?
    var filteredExercises = [String]()
    var isFiltering = false
    // nuevo
    var data = [String]()
    var searchActive = true
    var account: String?
    var direccion : String?
    var email: String?
    var filteredArray = [String]()
    var filtered:[String] = []
    
    var shouldShowSearchResults = false
    var datosFilter = [String]()
    var barraActiva = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // funcion nueva para obtener los nombre de los devices para el filtro
        fetchDevices()
          tableView.dataSource = self
        // update the list when we're told that a Position has been updated (this is the "Updated ... ago" message)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(DevicesViewController.reloadDevices),
                                               name: NSNotification.Name(rawValue: Definitions.PositionUpdateNotificationName),
                                               object: nil)
        
        // update the list when a Device has been updated (maybe the name has changed, or an addition/removal of a device)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(DevicesViewController.reloadDevices),
                                               name: NSNotification.Name(rawValue: Definitions.DeviceUpdateNotificationName),
                                               object: nil)
        
        if Definitions.isRunningOniPad {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                                     target: self,
                                                                     action: #selector(DevicesViewController.close))
        }
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        reloadDevices()
    }
    
    @objc func reloadDevices() {
        devices = WebService.sharedInstance.devices
        tableView.reloadData()
    }
    
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let d = devices {
          //return isFiltering == true ? filteredPersons!.count : d.count
        return !barraActiva ? data.count : datosFilter.count

        
        }
        
        return 0
    }
    
    
    func fetchDevices(_ onFailure: ((String) -> Void)? = nil)  {
        let account = WebService.sharedInstance.account
        let email = WebService.sharedInstance.email
        let password = WebService.sharedInstance.password
      let  serverURL  = "http://site1.i-gps.cl/CDCmobile/ios/"
       // let  serverURL  = "http://186.10.30.106:82/CDCmobile/ios/"
        
        let url = serverURL + "api/devices2/?accountID=" + account!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&userID=" + email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&password=" + password!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        Alamofire.request(url).responseJSON(completionHandler: { response in
            switch response.result {
                
                
            case .success(let JSON):
                if response.response!.statusCode != 200 {
                    if let fail = onFailure {
                        fail("Invalid server response")
                    }
                } else {
                    
                    if let data = JSON as? [[String : AnyObject]] {
                        
                        var devices = [Device]()
                        
                        for d in data {
                            let dd = Device()
                            dd.setValuesForKeys(d)
                            
                            devices.append(dd)
                            self.data.append(dd.name!)
                           // print(self.data)
                           
                        }
                        
                        
                    } else {
                        if let fail = onFailure {
                            fail("Server response was invalid")
                            print("no hay conexion")
                        }
                    }
                }
                
            case .failure(let error):
                let ac = UIAlertController(title: "No Hay conexion con el servidor ", message: "verifique su conexión o  si sus datos de sesión fueron actualizados", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                ac.addAction(okAction)
                self.present(ac, animated: true, completion: nil)
                if let fail = onFailure {
                    fail(error.localizedDescription)
                }
            }
        })
    }
 
    // esta funcion realiza el filtro de la lista
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            datosFilter = data.filter { $0.lowercased().contains(searchText.lowercased()) }
            barraActiva = true
        } else {
            datosFilter.removeAll(keepingCapacity: true)
            barraActiva = false
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        datosFilter.removeAll(keepingCapacity: true)
        barraActiva = false
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
     
      
        if let d = devices {
            let device = d[(indexPath as NSIndexPath).row]
            if !barraActiva {
               
            
                print(data)
                cell.textLabel!.text = device.name
                var lu = device.mostRecentPositionTimeString
                if lu.characters.count > 0 {
                    lu = lu.lowercased()
                    cell.detailTextLabel!.text = "Updated \(lu) ago"
                } else {
                    cell.detailTextLabel!.text = ""
                }

               
               
            } else {
                cell.textLabel?.text = datosFilter[indexPath.row]
            }
            
        }
           
         
           
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dvc = segue.destination as? DeviceInfoViewController {
            let idxp = tableView.indexPathForSelectedRow
            
            
            
            if let d = devices {
                
                if !barraActiva {
                    
                 dvc.device = d[(idxp! as NSIndexPath).row]
                    
                    
                } else {
                    let dispositivo2 = datosFilter[(idxp! as NSIndexPath).row]
                    for d in devices! {
                        if(d.name == dispositivo2)
                        {
                             dvc.device = d
                          
                        }
                        
                    }
                    
                }
                
                    
                
                
            }
            
            
            
            
        }
    }
}

