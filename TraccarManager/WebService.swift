/* Código de Jonathan León */

import Foundation
import Alamofire
import SocketIO
import SocketRocket
private struct NewsAPIStruct:Decodable
{
    let headlines:[Headlines];
}
// el socket  necesita una estructura
private struct Headlines:Decodable
{

    let accountID: String;
    
    
    init (json: [String: Any])
    {

        accountID = json ["accountID"] as? String ?? "";
        
    };
};






class WebService: UIViewController {
    private var tableView:UITableView!;
    private var headlinesArray = [String]();
    static let sharedInstance = WebService()
    
    var serverURL: String?
    var serverURL2: String?
    var account: String!
    var email: String?
    var password: String?
    var dato: String!
    // comunicacion de 3 vias
    let manager = SocketManager(socketURL: URL(string: "http://200.29.19.235:8000")!, config: [.log(true), .compress])
 // let manager = SocketManager(socketURL: URL(string: "http://localhost:8001")!,config: [.log(true),.connectParams(["accountID":"aerocardal"])])
    private var socket:SocketIOClient!
    
    
    // este socket es de la version traccar
    fileprivate var socket2: SRWebSocket?
    
    // map of device id (string) -> device
    // mapa de la identificación del dispositivo (cadena) -> dispositivo
    fileprivate var allDevices: NSMutableDictionary = NSMutableDictionary()
    
    // map of device id (string) -> position
    //
    // this provides an easy way of maintaining only the most-recent position
    // for each device
    
    // mapa de la identificación del dispositivo (cadena) -> posición
    //
    // esto proporciona una manera fácil de mantener solo la posición más reciente
    // para cada dispositivo
    fileprivate var allPositions: NSMutableDictionary = NSMutableDictionary()
     fileprivate var allPositions2: NSMutableDictionary = NSMutableDictionary()
    var positions: [Position] {
        get {
            return allPositions.allValues as! [Position]
        }
    }
    var positions2: [Position] {
        get {
            return allPositions.allValues as! [Position]
        }
    }
    var devices: [Device] {
        get {
            return allDevices.allValues as! [Device]
        }
    }
    
    // MARK: websocket
    
    
    /*  override func viewDidLoad() {
     
     super.viewDidLoad()
     
     
     
     }
     */
    
    // creacion de socket
    func getSocket()
    {
        self.socket = manager.defaultSocket;
        self.setSocketEvents();
        self.socket.connect();
        self.getHeadlines();  // llama al metodo para conexion
    }
    
    func getHeadlines()
    {

        reconnectWebSocket2();
        MapViewController.sharedInstance2.refreshPositions();
        DeviceInfoViewController.sharedInstance3.refreshPositions();
        dato = account
        // print("esto es " + account!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines));
       let jsonURLString:String = "http://200.29.19.235:3000/head/?accountID=\(account ?? "")"
        
       // let jsonURLString:String = "http://localhost:3006/head/?accountID=aerocardal"
        guard let url = URL(string: jsonURLString) else
        {return}
        //  print("color: \(account ?? "")")
        //  print("http://localhost:3000/head/?accountID=\(account ?? "")");
        
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            
            guard let data = data else { return }
            
            do{
                let newsAPIStruct = try
                    JSONDecoder().decode(NewsAPIStruct.self, from: data)
                
                for item in newsAPIStruct.headlines
                {
                    //self.headlinesArray.append (item.token);
                    self.headlinesArray.append (item.accountID);
                    // self.headlinesArray.append (item.headline);
                  // print("esto se supone que imprime\n"+item.accountID);
                    
                    
                    
                    // self.headlinesArray.append (item.token);
                };
                
                DispatchQueue.main.async(execute: {
                    // self.tableView.reloadData()
                })
            } catch let jsonErr
            {
                print ("error: ", jsonErr)
            }
            }.resume();
    };
    
    // verifica posible actualizaciones
    private func setSocketEvents()
    {
        self.socket.on(clientEvent: .connect) {data, ack in
            print("socket connected");
        };
        
        self.socket.on("posiciones actualizadas") {data, ack in
            self.getHeadlines();
        };
    };
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    };
    
    
    
    
    
    
    func disconnectWebSocket() {
        // close and tidy if we already had a socket
        
        // cerca y ordenado si ya teníamos un socket
        /*  if let s = socket {
         s.disconnect()
         
         socket = nil
         }*/
    }
    
    
    
    func webSocket(_ webSocket: SRWebSocket, didFailWithError error: Error) {
        reconnectWebSocket2()
        //reconnectWebSocket()
    }
    
    
    func webSocket(_ webSocket: SRWebSocket, didReceiveMessageWith string: String) {
        if let data = string.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                if let p = json["positions"] as? [[String: AnyObject]] {
                    parsePositionData(p)
                }
            }
            catch {
                print("error parsing JSON")
            }
        }
    }
    /*  func reconnectWebSocket() {
     
     disconnectWebSocket()
     
     // if the server URL hasn't been set, there's no point continuing
     
     // si no se ha establecido el URL del servidor, no tiene sentido continuar
     guard serverURL != nil else {
     return
     }
     
     //let urlString = "\(serverURL!)api/socket"
     let urlString = "http://186.10.30.106:82/CDCmobile/ios/:82"
     // let urlString = "http://186.10.30.106"
     
     socket = SRWebSocket(url: URL(string: urlString)!)
     
     
     if let s = socket {
     let cookiePath = "http://186.10.30.106:82/CDCmobile/ios/api/socket/"
     s.requestCookies = HTTPCookieStorage.shared.cookies(for: URL(string: cookiePath)!)
     s.delegate = self
     s.open()
     }
     }*/
    //prueba
    func reconnectWebSocket2(_ onFailure: ((String) -> Void)? = nil) {
        disconnectWebSocket()
        
        guard serverURL != nil else {
            return
        }
        let url = serverURL! + "api/positions2/?accountID=" + account!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&userID=" + email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&password=" + password!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        Alamofire.request(url).responseJSON(completionHandler: { response in
            switch response.result {
            case .success(let JSON):
                if response.response!.statusCode != 200 {
                    if let fail = onFailure {
                        fail("Invalid server response")
                    }
                } else {
                    
                    if let data = JSON as? [[String : AnyObject]] {
                        
                        self.parsePositionData(data)
                        
                        
                        
                        
                        
                        
                    } else {
                        if let fail = onFailure {
                            fail("Server response was invalid")
                            print("no hay conexion")
                        }
                    }
                }
                
            case .failure(let error):
                let ac = UIAlertController(title: "No Hay conexion con el servidor ", message: "verifique su conexión o  si sus datos de sesión fueron actualizados", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                ac.addAction(okAction)
                self.present(ac, animated: true, completion: nil)
                self.present(ac, animated: true, completion: nil)
                if let fail = onFailure {
                    fail(error.localizedDescription)
                }
            }
        })
        
    }
    
    
    
   
    
    
    
    
    
    // MARK: fetch
    
    fileprivate func parsePositionData(_ data: [[String : AnyObject]]) {
        
        var positions = [Position]()
        
        for p in data {
            let pp = Position()
            pp.setValuesForKeys(p)
            positions.append(pp)
            
            allPositions.setValue(pp, forKey: (pp.deviceId?.stringValue)!)
        }
        
        // tell everyone that the positions have been updated
        // decirle a todos que las posiciones han sido actualizadas
        NotificationCenter.default.post(name: Notification.Name(rawValue: Definitions.PositionUpdateNotificationName), object: nil)
    }
    fileprivate func parsePositionData2(_ data: [[String : AnyObject]]) {
        
        var positions = [Position]()
        
        for p in data {
            let pp = Position()
            pp.setValuesForKeys(p)
            positions.append(pp)
            
            allPositions2.setValue(pp, forKey: (pp.deviceId?.stringValue)!)
        }
        
        // tell everyone that the positions have been updated
        // decirle a todos que las posiciones han sido actualizadas
        NotificationCenter.default.post(name: Notification.Name(rawValue: Definitions.PositionUpdateNotificationName), object: nil)
    }
    //    func authenticate(_ serverURL: String, account: String, email: String, password: String, onFailure: ((String) -> Void)? = nil, onSuccess: @escaping (User) -> Void) {
    
    
    func fetchDevices(_ onFailure: ((String) -> Void)? = nil, onSuccess: @escaping ([Device]) -> Void) {
        
        guard serverURL != nil else {
            return
        }
        
        
        let url = serverURL! + "api/devices2/?accountID=" + account!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&userID=" + email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&password=" + password!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        Alamofire.request(url).responseJSON(completionHandler: { response in
            switch response.result {
                
                
            case .success(let JSON):
                if response.response!.statusCode != 200 {
                    if let fail = onFailure {
                        fail("Invalid server response")
                    }
                } else {
                    
                    if let data = JSON as? [[String : AnyObject]] {
                        
                        var devices = [Device]()
                        
                        for d in data {
                            let dd = Device()
                            dd.setValuesForKeys(d)
                            devices.append(dd)
                           /* print("este es el estado" + dd.status!)
                            print( dd.id!)
                            print( dd.groupId!)
                            print(dd.uniqueId!)
                            print("este es el estado" + dd.name!)*/
                            self.allDevices.setValue(dd, forKey: (dd.id?.stringValue)!)
                        }
                        
                        // tell everyone that the devices have been updated
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Definitions.DeviceUpdateNotificationName), object: nil)
                        
                        onSuccess(devices)
                        
                    } else {
                        if let fail = onFailure {
                            fail("Server response was invalid")
                            print("no hay conexion")
                        }
                    }
                }
                
            case .failure(let error):
                let ac = UIAlertController(title: "No Hay conexion con el servidor ", message: "verifique su conexión o  si sus datos de sesión fueron actualizados", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                ac.addAction(okAction)
                self.present(ac, animated: true, completion: nil)
                if let fail = onFailure {
                    fail(error.localizedDescription)
                }
            }
        })
    }
    

    func positionByDeviceId(_ deviceId: NSNumber) -> Position? {
        if let p = allPositions[deviceId.stringValue] {
            return p as? Position
        }
        return nil
    }
    
    // utility function to get a device by ID
    
    // función de utilidad para obtener un dispositivo por ID
    func deviceById(_ id: NSNumber) -> Device? {
        if let d = allDevices[id.stringValue] {
            return d as? Device
        }
        return nil
    }
 

    // MARK: auth
    
    //   func authenticate(_ serverURL: String, account: String, email: String, password: String, onFailure: ((String) -> Void)? = nil, onSuccess: @escaping (User) -> Void) {
    func authenticate( account: String, email: String, password: String, onFailure: ((String) -> Void)? = nil, onSuccess: @escaping (User) -> Void) {
        
        // clear any devices/positions from the previous session
        allPositions = NSMutableDictionary()
        allDevices = NSMutableDictionary()
        
        //        guard (serverURL.lowercased().hasPrefix("http://") || serverURL.lowercased().hasPrefix("https://")) else {
        //            if let fail = onFailure {
        //                fail("Server URL must begin with either http:// or https://")
        //            }
        //            return
        //        }
        
        //var url = serverURL
        var url = "http://site1.i-gps.cl/CDCmobile/ios/"
        //var url = "http://186.10.30.106:82/CDCmobile/ios/"
        let urlaccount = account
        let urlemail = email
        let urlpassword = password
        
        
        //      if !serverURL.hasSuffix("/") {
        //          url = url + "/"
        //      }
        
        self.serverURL = url
        self.account = urlaccount
        self.email = urlemail
        self.password = urlpassword
        
        url = url + "api/session"
        
        let parameters = [
            "accountID" : account,
            "userID" : email,
            "password": password
        ]
        
        Alamofire.request(url, method: .get, parameters: parameters).responseJSON(completionHandler: { response in
            switch response.result {
                
            case .success(let JSON):
                if response.response!.statusCode != 200 {
                    if let fail = onFailure {
                        fail("Datos invalidos")
                    }
                } else {
                    
                    if let data = JSON as? [String : AnyObject] {
                        let u = User.sharedInstance
                        u.setValuesForKeys(data)
                        
                        self.reconnectWebSocket2()
                        
                        
                        // tell everyone that the user has logged in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Definitions.LoginStatusChangedNotificationName), object: nil)
                        
                        onSuccess(u)
                        self.getSocket();
                    } else {
                        if let fail = onFailure {
                            fail("Respuesta del servidor invalida")
                            print("no hay conexion")
                        }
                    }
                }
                
            case .failure(let error):
                if let fail = onFailure {
                    fail(error.localizedDescription)
                }
            }
        })
    }
    
}

