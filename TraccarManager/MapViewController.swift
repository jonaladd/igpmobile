
import UIKit
import MapKit
import SocketIO
import Alamofire
import UserNotifications
import CoreLocation
class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UNUserNotificationCenterDelegate{
   // let myNotification = Notification.Name(rawValue:"MyNotification")
     @IBOutlet weak var imageView: UIImageView!
    var timer: DispatchSourceTimer?
    var serverURL: String?
    var color: UIColor?
    var account: String?
    var geo: String? = ""
     var consulta: String?
      var mapa: String?
    var gps: String?
    var mapa2: String?
    var alarma: Bool = false
     var ubication: Bool = false
    var email: String?
    var password: String?
    var velocidad: String?
    var devices2: [Device]?
    let TCDefaultsAccountKey = "DefaultsAccountKey"
    let TCDefaultsEmailKey = "DefaultsEmailKey"
    let TCDefaultsPasswordKey = "DefaultsPasswordKey"
    let TCDefaultsAlertKey = "DefaultsAlertKey"
    let TCDefaultsMapsKey = "DefaultsMapsKey"
     let TCDefaultsGpsKey = "DefaultsGpsKey"
    static let sharedInstance2 = MapViewController()
    @IBOutlet var mapView: MKMapView?
    private var headlinesArray = [String]();
    fileprivate var allDevices: NSMutableDictionary = NSMutableDictionary()
    var count = 0;
    var contador:Int = 0
    var year:Int = 2
   var contadorDetenidos:Int = 0
   var contadorMovimiento:Int = 0
   var contadorFalla:Int = 0
   var  contadorPanico:Int = 0
   var contadorApagado:Int = 0
   var contadorReposo:Int = 0
   var  contadorEncendido:Int = 0
    var contadorEntrada:Int = 0
   var  contadorSalida:Int = 0
   var   contadorTransito:Int = 0
   var   contadorTotal:Int = 0
    var name: String?
   var locationManager:CLLocationManager!
    /*  var account = WebService.sharedInstance.account;
     
     
    
     
     private var socket:SocketIOClient!*/
    fileprivate var devices: [Device] = []
    fileprivate var allPositions: NSMutableDictionary = NSMutableDictionary()
    
    fileprivate var positions: [Position] = []
    
    private var positions2: [Position] {
        get {
            return allPositions.allValues as! [Position]
        }
    }
    
    // controls whether the map view should center on user's default location
    // when the view appears. we use this variable to prevent re-centering the
    // map every single time this view appears
    
    // controla si la vista del mapa debe centrarse en la ubicación predeterminada del usuario
    // cuando aparece la vista. usamos esta variable para evitar que se vuelva a centrar
    // mapa cada vez que aparece esta vista
    fileprivate var shouldCenterOnAppear: Bool = true
    
    // if a map pin is tapped by the user, a reference will be stored here
    
    // si un pin de mapa es aprovechado por el usuario, se guardará una referencia aquí
    var selectedAnnotation: PositionAnnotation?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        UNUserNotificationCenter.current().delegate = self
       /* let t = RepeatingTimer(timeInterval: 3)
        t.eventHandler = {
            self.scheduleNotification(tile: "hola", sub: "como estas");
            print("esto es repetitivo")
        }*/
        //t.resume()
          let d = UserDefaults.standard
        if let e = d.string(forKey: TCDefaultsMapsKey ) {
            mapa = e
           
            
        }
        if let d = d.string(forKey: TCDefaultsGpsKey ) {
            gps = d
            
            
        }
        if(mapa != nil)
        {
            if(mapa == "MKMapType.standard"){
                mapView?.mapType = MKMapType.standard;
            }
            if(mapa == "MKMapType.satellite"){
                mapView?.mapType = MKMapType.satellite;
            }
            if(mapa == "MKMapType.hybrid"){
                mapView?.mapType = MKMapType.hybrid;
            }
        }
        else{
            mapView?.mapType = MKMapType.standard;
            let d = UserDefaults.standard
            
            d.setValue("MKMapType.standard", forKey: TCDefaultsMapsKey)
            // d.setValue("no", forKey: TCDefaultsAlertKey)
            d.synchronize()
        }
        locationManager = CLLocationManager()
        actualizar()
        
        mapView?.showsPointsOfInterest = true;
        mapView?.showsTraffic = true;
        mapView?.showsCompass = true;
        mapView?.isPitchEnabled = true
        mapView?.isRotateEnabled = true
        //startTimer()
        
        
        if(gps != nil)
        {
            if(gps == "true"){
            self.ubication = true
            self.mapView?.showsUserLocation = true;
            }
        }
        else{
           
            let d = UserDefaults.standard
            
            d.setValue("false", forKey: TCDefaultsGpsKey)
            // d.setValue("no", forKey: TCDefaultsAlertKey)
            d.synchronize()
        }
        
        zoomToAllButtonPressed();
      
        if let e = d.string(forKey: TCDefaultsAlertKey ) {
            consulta = e
            
            
        }
        print(consulta)
        if(consulta == "si"){
            startTimer()
            alarma = true;
        }else{
            stopTimer()
            alarma = false
        }
       
     
    }
    func actualizar() {
        devices2 = WebService.sharedInstance.devices
    }
    public func startTimer() {
        
      
       
        let queue = DispatchQueue(label: "com.firm.app.timer", attributes: .concurrent)
        
        timer?.cancel()        // cancel previous timer if any
        
        timer = DispatchSource.makeTimerSource(queue: queue)
        
      //  timer?.schedule(deadline: .now(), repeating: .seconds(5), leeway: .milliseconds(100))
        
        // or, in Swift 3:
        //
        timer?.scheduleRepeating(deadline: .now(), interval: .seconds(60), leeway: .seconds(1))
        
        timer?.setEventHandler { [weak self] in // `[weak self]` only needed if you reference `self` in this closure and you want to prevent strong reference cycle
            print(Date())
            self?.fetchDevices2()
           
        }
        
        timer?.resume()
        
        

    }
  
    
    
    private func stopTimer() {
        timer?.cancel()
        timer = nil
        let d = UserDefaults.standard
        // d.setValue(self.serverField!.text!, forKey: TCDefaultsServerKey)
        
        d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      
        
        
        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
        
       
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
       // print(response.actionIdentifier.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
    
      
        if response.actionIdentifier == "quiz" {
            
           // print("pepe")
            self.performSegue(withIdentifier: "show", sender:self)
        }
       /* else{
            print("dffdfdfdfdfdfdfdgfdgdfgfdgfdgfdgfdgfdgfd")
            self.performSegue(withIdentifier: "show", sender:self)
        }*/
      
       
    }

    func scheduleNotification(tile: String, sub: String, m: String) {
        
        if #available(iOS 10.0, *) {
            
            
            let content = UNMutableNotificationContent()
            
            content.title = "Alerta!"
            content.subtitle = NSString.localizedUserNotificationString(forKey: tile, arguments: nil)
            content.body = NSString.localizedUserNotificationString(forKey: sub, arguments: nil)
            //sonido default
           //content.sound = UNNotificationSound.default()
            
           content.badge = 1
           
            content.categoryIdentifier = m
           // content.categoryIdentifier = tile
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
          
            let requestIdentifier = m
            let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
         
            UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
                // handle error
            })
            
            if(m == "quiz"){
                 let answerOne = UNNotificationAction(identifier: m, title: "Ver todas mis notifaciones?", options: [.foreground])
                
                
                 let quizCategory = UNNotificationCategory(identifier: m, actions: [answerOne], intentIdentifiers: [], options: [])
                 UNUserNotificationCenter.current().setNotificationCategories([quizCategory])
                   self.contador = 0;
            }/*else{
                let answerTwo = UNNotificationAction(identifier: m, title: "Ver?", options: [.foreground])
                let quizCategory2 = UNNotificationCategory(identifier: m, actions: [answerTwo], intentIdentifiers: [], options: [])
                UNUserNotificationCenter.current().setNotificationCategories([quizCategory2])
                self.contador = 0;
                
            }*/
         
        
               } else {
            // Fallback on earlier versions
        }
        
    }
   
    func fetchDevices2(_ onFailure: ((String) -> Void)? = nil)  {
        var account: String!
        var email: String?
        var password: String?
        let d = UserDefaults.standard
        //       if let s = d.string(forKey: TCDefaultsServerKey) {
        //           self.serverField.text = s
        //       }
        if let e = d.string(forKey: TCDefaultsEmailKey) {
         email = e
            
            
        }
        if let e = d.string(forKey: TCDefaultsAccountKey) {
             account  = e
            
        }
        if let e = d.string(forKey: TCDefaultsPasswordKey) {
           password = e
            
            
        }
        
       // print(email)
      //  print(account)
         print(password)
        let dd = Position()
      //  let account = WebService.sharedInstance.account
      //  let email = WebService.sharedInstance.email
       // let password = WebService.sharedInstance.password
        let  serverURL  = "http://site1.i-gps.cl/CDCmobile/android/"
       // let  serverURL  = "http://186.10.30.106:82/CDCmobile/ios/"
        
        let url = serverURL + "api/notification/?accountID=" + account!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&userID=" + email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&password=" + password!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        Alamofire.request(url).responseJSON(completionHandler: { response in
            switch response.result {
                
                
            case .success(let JSON):
                if response.response!.statusCode != 200 {
                    if let fail = onFailure {
                        fail("Invalid server response")
                    }
                } else {
                    
                    if let data = JSON as? [[String : AnyObject]] {
                        var positions = [Position]()
                        
                        
                        for d in data {
                            let dd = Position()
                            
                            dd.setValuesForKeys(d)
                            positions.append(dd)
                            
                            for d in self.devices {
                               // print(d.deviceId)
                               // print(d.name)
                                if(d.deviceId == dd.deviceId2){
                                    
                                    
                                    self.name = d.name
                                }
                                else{
                                    //self.name = "no hay info"
                                }
                                
                            }
                           
                            if(self.contador>3)
                            {
                                
                                var title = "Tiene Notificaciones pendientes en el"
                                var sub = dd.fixTime!
                                
                                let m = "quiz"
                                self.scheduleNotification(tile: title, sub: sub, m: m);
                              
                            }
                           // print("si pasa por aca amigo mio del alma pero no pasa nada")
                            
                            
                            if(dd.alarmaString == "En Reposo")
                            {
                                if(self.name != nil){
                                var title = "En Reposo:  el vehículo " + self.name! + "  está en reposo en el \t" + dd.fixTime!
                                
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                
                                }
                            }
                            if(dd.alarmaString == "Exceso de Reposo")
                            {
                                if(self.name != nil){
                                var title = "Exceso de Reposo:  el vehículo " + self.name! + " excedio el tiempo limite de reposo en el  en el \t" + dd.fixTime!
                               
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.contador = self.contador + 1
                                }
                            }
                            if(dd.alarmaString == "Entrada en Geozona")
                            {
                                if(self.name != nil){
                                var title = "Entrada en Geozona: el vehículo " + self.name! + " entro en la geozona en el \t" + dd.geozoneID! + dd.fixTime!
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            if(dd.alarmaString == "Motor Encendido")
                            {
                                if(self.name != nil){
                                var title = "Motor Encendido: el vehículo "  + self.name! + " encendio el motor  en el " + dd.fixTime!
                               let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            
                            if(dd.alarmaString == "Motor Apagado")
                            {
                                if(self.name != nil){
                                var title = "Motor Apagado: el vehículo " +  self.name! + " apago el motor en el " + dd.fixTime!
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            
                            
                            if(dd.alarmaString == "Salida de Geozona")
                            {
                                if(self.name != nil){
                                var title = "Salida de Geozona: el vehículo " +  self.name! + " Salio de la geozona \t" + dd.geozoneID! + " en el " + dd.fixTime!
                                
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            if(dd.alarmaString == "Transito en Geozona")
                            {
                                if(self.name != nil){
                                var title = "Transito en Geozona: el vehículo " + self.name! + " Transita en ale geozona  \t" + dd.geozoneID! + " en el " + dd.fixTime!
                                
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            if(dd.alarmaString == "Tiempo en Geozonas")
                            {
                                if(self.name != nil){
                                var title = "Tiempo en Geozonas: el vehículo" + self.name! + " Excedio el tiempo en la geozona \t" + dd.geozoneID! + " en el " + dd.fixTime!
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            if(dd.alarmaString == "Boton de Panico")
                            {
                                if(self.name != nil){
                                var title = "Boton de Panico: el vehículo " + self.name! + " presiono el botón de pánico en el \t" + dd.fixTime!
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            if(dd.alarmaString == "Apertura de Puertas")
                            {
                                if(self.name != nil){
                                var title = "Apertura de Puertas: el vehículo " + self.name! + " abrio las puertas en el \t" + dd.fixTime!
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            if(dd.alarmaString == "Cierre de Puertas")
                            {
                                if(self.name != nil){
                                var title = "Cierre de Puertas: el vehículo " + self.name! + " cerro las puerta en el \t" + dd.fixTime!
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            if(dd.alarmaString == "Falla de Energia")
                            {
                                if(self.name != nil){
                                var title = "Falla de Energía: el vehículo " + self.name! + " sufrio un falla de energía en el  \t" + dd.fixTime!
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            if(dd.alarmaString == "Velocidad")
                            {
                                if(self.name != nil){
                                var title = "Exceso de velocidad: el vehículo " + self.name! + " excedio la velocidad maxima en el \t" + dd.fixTime!
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            if(dd.alarmaString == "Velocidad maxima en Geozona")
                            {
                                if(self.name != nil){
                                var title = "Exceso de velocidad en geozona: el vehículo " + self.name! + " excedio la velocida maxima en la geozona \t" + dd.geozoneID! + "en el" + dd.fixTime!
                                let m = dd.alarmaString! + self.name!
                                self.scheduleNotification(tile: self.name!, sub: title, m: m);
                                    self.count = self.count + 1;
                                    UIApplication.shared.applicationIconBadgeNumber = self.count
                                    self.contador = self.contador + 1
                                }
                            }
                            
                            
                            
                            
                            
                            
                        }
                        
                        
                    } else {
                        if let fail = onFailure {
                            fail("Server response was invalid")
                        }
                    }
                }
                
            case .failure(let error):
                let ac = UIAlertController(title: "No Hay conexion con el servidor ", message: "verifique su conexión o  si sus datos de sesión fueron actualizados", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                ac.addAction(okAction)
                self.present(ac, animated: true, completion: nil)
                if let fail = onFailure {
                    fail(error.localizedDescription)
                }
            }
        })
 
    }
    
    
    
    @IBAction func alarmaButton() {
        
        if (alarma == false || consulta == "no")
        {
            let otherAlert = UIAlertController(title: " Activar Las Notificaciones ?", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            /* let printSomething = UIAlertAction(title: "Print", style: UIAlertActionStyle.default) { _ in
             print("We can run a block of code." )
             }
             */
            let callFunction = UIAlertAction(title: "Si", style: UIAlertActionStyle.destructive, handler: myActivar)
            
            let dismiss = UIAlertAction(title: "no", style: UIAlertActionStyle.cancel, handler: nil)
            
            
            otherAlert.addAction(callFunction)
            otherAlert.addAction(dismiss)
            
            present(otherAlert, animated: true, completion: nil)
            
        }
        
        else {
            
            
            
            
            let otherAlert = UIAlertController(title: " Desactivar Las Notificaciones ?", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            /* let printSomething = UIAlertAction(title: "Print", style: UIAlertActionStyle.default) { _ in
             print("We can run a block of code." )
             }
             */
            let callFunction = UIAlertAction(title: "Si", style: UIAlertActionStyle.destructive, handler: myDesactivar )
            
            let dismiss = UIAlertAction(title: "no", style: UIAlertActionStyle.cancel, handler: nil)
            
            
            otherAlert.addAction(callFunction)
            otherAlert.addAction(dismiss)
            
            present(otherAlert, animated: true, completion: nil)
            
        }
   
        
    }
    
    
    func myActivar(alert: UIAlertAction){
        let d = UserDefaults.standard
        // d.setValue(self.serverField!.text!, forKey: TCDefaultsServerKey)
        
        d.setValue("si", forKey: TCDefaultsAlertKey)
        d.synchronize()
        startTimer();
        alarma = true;
       
        
    }
    
    func myDesactivar(alert: UIAlertAction){
        let d = UserDefaults.standard
        // d.setValue(self.serverField!.text!, forKey: TCDefaultsServerKey)
        
        d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
        stopTimer();
        alarma = false;
        
    }
    
    func satelite(alert: UIAlertAction){
        mapView?.mapType = MKMapType.satellite;
        let d = UserDefaults.standard
       
        d.setValue("MKMapType.satellite", forKey: TCDefaultsMapsKey)
        // d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
        
    }
    func standar(alert: UIAlertAction){
        mapView?.mapType = MKMapType.standard;
        let d = UserDefaults.standard
        
        d.setValue("MKMapType.standard", forKey: TCDefaultsMapsKey)
        // d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
    }
    func hybrid(alert: UIAlertAction){
        mapView?.mapType = MKMapType.hybrid;
        let d = UserDefaults.standard
        
        d.setValue("MKMapType.hybrid", forKey: TCDefaultsMapsKey)
        // d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
    }
    func changeMap(){
       reiniciar()
        
        let otherAlert = UIAlertController(title: "ELIGA UNA OPCIÓN", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        /* let printSomething = UIAlertAction(title: "Print", style: UIAlertActionStyle.default) { _ in
         print("We can run a block of code." )
         }
         */
        let callstandar = UIAlertAction(title: "Cambiar Mapa", style: UIAlertActionStyle.default, handler: changeMap2)
        
        let dismiss = UIAlertAction(title: "Cerrar", style: UIAlertActionStyle.destructive, handler: nil)
    
        let callhybrid2 = UIAlertAction(title: "Información De Estados", style: UIAlertActionStyle.default, handler: info)
        
        let callhybrid3 = UIAlertAction(title: "Información De Geozonas", style: UIAlertActionStyle.default, handler: info2)
     let callhybrid4 = UIAlertAction(title: "Mi Ubicación", style: UIAlertActionStyle.default, handler: Ubicacion)
        otherAlert.addAction(callstandar)
        otherAlert.addAction(callhybrid2)
         otherAlert.addAction(callhybrid3)
         otherAlert.addAction(callhybrid4)
        otherAlert.addAction(dismiss)
        
        present(otherAlert, animated: true, completion: nil)
       
        geozonas()
        
    }
    
    func ubicacion(alert: UIAlertAction){
        self.locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
               // print("No access")
                self.ubication = true
                self.mapView?.showsUserLocation = true;
                
            self.mapView?.showsUserLocation = true;
                
            case .authorizedAlways, .authorizedWhenInUse:
               // print("Access")
                 self.ubication = true
                 self.mapView?.showsUserLocation = true;
                self.refreshPositions()
               
                 ubicacion44()
                
                self.mapView?.showsUserLocation = true;
                
                
            }
        } else {
           // print("servicio no disponible")
        }
        
     
      
       // self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: false)
          self.mapView?.showsUserLocation = true;
        let d = UserDefaults.standard
        
        d.setValue("true", forKey: TCDefaultsGpsKey)
        // d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
   
        
    }
    
    func ubicacion44(){
      //  self.ubication = true
         // self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: false)
        // self.mapView?.showsUserLocation = true;


        self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: false)
         self.refreshPositions()
       
    }
    func ubicacion2(alert: UIAlertAction){
        
     
         //  locationManager.stopUpdatingLocation()
        
         self.ubication = false
         self.mapView?.showsUserLocation = false;
        self.refreshPositions()
        self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: false)
        let d = UserDefaults.standard
        
        d.setValue("false", forKey: TCDefaultsGpsKey)
        // d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
        
    }
    @IBAction func Ubicacion(alert: UIAlertAction) {
        
        
        let otherAlert = UIAlertController(title: "Mostrar mi ubicación en el mapa?", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        /* let printSomething = UIAlertAction(title: "Print", style: UIAlertActionStyle.default) { _ in
         print("We can run a block of code." )
         }
         */
        let callFunction = UIAlertAction(title: "si", style: UIAlertActionStyle.destructive, handler: ubicacion)
         let callFunction2 = UIAlertAction(title: "no", style: UIAlertActionStyle.destructive, handler: ubicacion2)
        let dismiss = UIAlertAction(title: "cerrar", style: UIAlertActionStyle.cancel, handler: nil)
        
        
        otherAlert.addAction(callFunction)
         otherAlert.addAction(callFunction2)
        otherAlert.addAction(dismiss)
        
        present(otherAlert, animated: true, completion: nil)
        
        
        
        
    }
  
    func geozonas(_ onFailure: ((String) -> Void)? = nil)  {
        let dd = Position()
        let account = WebService.sharedInstance.account
        let email = WebService.sharedInstance.email
        let password = WebService.sharedInstance.password
        let  serverURL  = "http://site1.i-gps.cl/CDCmobile/ios/"
        
        
        let url = serverURL + "api/positions2/?accountID=" + account!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&userID=" + email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&password=" + password!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        Alamofire.request(url).responseJSON(completionHandler: { response in
            switch response.result {
                
                
            case .success(let JSON):
                if response.response!.statusCode != 200 {
                    if let fail = onFailure {
                        fail("Invalid server response")
                    }
                } else {
                    
                    if let data = JSON as? [[String : AnyObject]] {
                        var positions = [Position]()
                        
                        
                        for d in data {
                            let dd = Position()
                            
                            
                            dd.setValuesForKeys(d)
                            positions.append(dd)
                            
                           if(dd.geozoneID != nil && dd.geozoneID != "")
                           {
                         self.geo = self.geo! + (dd.device?.name)! + " Está En " + dd.geozoneID! + "\r\n"
                            
                          //  self.geo = self.geo! + "ffddf"
                            }
                            
                            if(dd.estadoString == "En Movimiento")
                            {
                                
                                self.contadorMovimiento = self.contadorMovimiento + 1
                            }
                            if(dd.estadoString == "Detenido")
                            {
                                
                                self.contadorDetenidos = self.contadorDetenidos + 1
                            }
                            if(dd.estadoString == "En reposo")
                            {
                                self.contadorReposo =  self.contadorReposo + 1
                                
                            }
                            if(dd.estadoString == "Motor Apagado")
                            {
                                self.contadorApagado =  self.contadorApagado + 1
                                
                            }
                            if(dd.estadoString == "Falla De Energía")
                            {
                                self.contadorFalla = self.contadorFalla + 1
                                
                            }
                            if(dd.estadoString == "Boton De Panico")
                            {
                                
                                self.contadorPanico =  self.contadorPanico + 1
                                
                            }
                            if(dd.estadoString == "Entrada A Geozona")
                            {
                                self.contadorEntrada = self.contadorEntrada + 1
                                
                            }
                            if(dd.estadoString == "Salida De Geozona")
                            {
                                
                                self.contadorSalida = self.contadorSalida + 1
                            }
                            if(dd.estadoString == "Transito Entre Geozonas")
                            {
                                
                                self.contadorTransito = self.contadorTransito + 1
                            }
                            if(dd.estadoString == "Transito Entre Geozonas")
                            {
                                
                                self.contadorTransito = self.contadorTransito + 1
                            }
                            if(dd.estadoString == "Motor encendido")
                            {
                                
                                
                                self.contadorEncendido = self.contadorEncendido + 1
                            }
                            if(dd.deviceId != nil )
                            {
                                
                                
                                self.contadorTotal = self.contadorTotal + 1
                            }
                            
                           
                            
                            
                            
                            
                        }
                        
                        
                    } else {
                        if let fail = onFailure {
                            fail("Server response was invalid")
                        }
                    }
                }
                
            case .failure(let error):
                if let fail = onFailure {
                    fail(error.localizedDescription)
                }
            }
        })
    }
    
    func changeMap2(alert: UIAlertAction){
        let otherAlert = UIAlertController(title: "Visualizaciones Del Mapa", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        let d = UserDefaults.standard
        if let e = d.string(forKey: TCDefaultsMapsKey ) {
            mapa2 = e
            
            
        }
        /* let printSomething = UIAlertAction(title: "Print", style: UIAlertActionStyle.default) { _ in
         print("We can run a block of code." )
         }
         */
        if(mapa2 != nil)
        {
            if(mapa2 == "MKMapType.standard"){
                let callstandar = UIAlertAction(title: "Mapa Estandar              ✔", style: UIAlertActionStyle.default, handler: standar)
                
                let dismiss = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.destructive, handler: nil)
                let callhybrid  = UIAlertAction(title: "Mapa híbrido                 ", style: UIAlertActionStyle.default, handler: hybrid)
                let callhybrid2 = UIAlertAction(title: "Mapa Satelital               ", style: UIAlertActionStyle.default, handler: satelite)
                // relate actions to controllers
                //otherAlert.addAction(printSomething)
                otherAlert.addAction(callstandar)
                // otherAlert.addAction(callSatelite)
                otherAlert.addAction(callhybrid)
                otherAlert.addAction(callhybrid2)
                otherAlert.addAction(dismiss)
                
                present(otherAlert, animated: true, completion: nil)
            }
            else if(mapa2 == "MKMapType.satellite"){
                let callstandar = UIAlertAction(title: "Mapa Estandar               ", style: UIAlertActionStyle.default, handler: standar)
                
                let dismiss = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.destructive, handler: nil)
                let callhybrid  = UIAlertAction(title: "Mapa híbrido                ", style: UIAlertActionStyle.default, handler: hybrid)
                let callhybrid2 = UIAlertAction(title: "Mapa Satelital             ✔", style: UIAlertActionStyle.default, handler: satelite)
                // relate actions to controllers
                //otherAlert.addAction(printSomething)
                otherAlert.addAction(callstandar)
                // otherAlert.addAction(callSatelite)
                otherAlert.addAction(callhybrid)
                otherAlert.addAction(callhybrid2)
                otherAlert.addAction(dismiss)
                
                present(otherAlert, animated: true, completion: nil)
            }
           else if(mapa2 == "MKMapType.hybrid"){
                let callstandar = UIAlertAction(title: "Mapa Estandar              ", style: UIAlertActionStyle.default, handler: standar)
                
                let dismiss = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.destructive, handler: nil)
                let callhybrid  = UIAlertAction(title: "Mapa híbrido             ✔", style: UIAlertActionStyle.default, handler: hybrid)
                let callhybrid2 = UIAlertAction(title: "Mapa Satelital             ", style: UIAlertActionStyle.default, handler: satelite)
                // relate actions to controllers
                //otherAlert.addAction(printSomething)
                otherAlert.addAction(callstandar)
                // otherAlert.addAction(callSatelite)
                otherAlert.addAction(callhybrid)
                otherAlert.addAction(callhybrid2)
                otherAlert.addAction(dismiss)
                
                present(otherAlert, animated: true, completion: nil)
            }
           else{
                let callstandar = UIAlertAction(title: "Mapa Estandar    ✔", style: UIAlertActionStyle.default, handler: standar)
                
                let dismiss = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.destructive, handler: nil)
                let callhybrid  = UIAlertAction(title: "Mapa híbrido       ", style: UIAlertActionStyle.default, handler: hybrid)
                let callhybrid2 = UIAlertAction(title: "Mapa Satelital      ", style: UIAlertActionStyle.default, handler: satelite)
                // relate actions to controllers
                //otherAlert.addAction(printSomething)
                otherAlert.addAction(callstandar)
                // otherAlert.addAction(callSatelite)
                otherAlert.addAction(callhybrid)
                otherAlert.addAction(callhybrid2)
                otherAlert.addAction(dismiss)
                present(otherAlert, animated: true, completion: nil)
            }
        }
        else{
            
        }
        
        //  dos()
        
    }
    func info(alert: UIAlertAction){
        
        
        let mensaje =   "Cantidad Total:                " + "\(self.contadorTotal)" + " Móviles \r\n" +
                        "Detenidos:                        " + "\(self.contadorDetenidos)" + " Móviles \r\n" +
                        "En Movimiento:              " + "\(self.contadorMovimiento)" + " Móviles \r\n" +
                        "Falla De Energía:            " + "\(self.contadorFalla)" + " Móviles \r\n" +
                        "Botón De Panico:           " + "\(self.contadorPanico)" + " Móviles \r\n" +
                        "Motor Apagado:             " + "\(self.contadorApagado)" + " Móviles \r\n" +
                        "Motor Encendido:           " + "\(self.contadorEncendido)" + " Móviles \r\n" +
                        "En Reposo:                     " + "\(self.contadorReposo)" + " Móviles \r\n" +
                        "Entrada Geozona :         " + "\(self.contadorEntrada)" + " Móviles \r\n" +
                        "Salida De Geozona:        " + "\(self.contadorSalida)" + " Móviles \r\n" +
                        "Transito Entre Geozona:" + "\(self.contadorTransito)" + " Móviles \r\n"
        
        
        
        let otherAlert = UIAlertController(title: "INFORMACIÓN GENERAL DE ESTADOS", message: mensaje, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        /* let printSomething = UIAlertAction(title: "Print", style: UIAlertActionStyle.default) { _ in
         print("We can run a block of code." )
         }
         */
     
        
        let dismiss = UIAlertAction(title: "Cerrar", style: UIAlertActionStyle.destructive, handler: nil)
       
      
        // relate actions to controllers
        //otherAlert.addAction(printSomething)
      
       
        otherAlert.addAction(dismiss)
        
        present(otherAlert, animated: true, completion: nil)
        reiniciar();
        
    }
    
    
    func info2(alert: UIAlertAction){
        
        
        let mensaje = self.geo!
        let otherAlert = UIAlertController(title: "INFORMACIÓN DE MÓVILES EN GEOZONA", message: mensaje, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        /* let printSomething = UIAlertAction(title: "Print", style: UIAlertActionStyle.default) { _ in
         print("We can run a block of code." )
         }
         */
        
        let dismiss = UIAlertAction(title: "Cerrar", style: UIAlertActionStyle.destructive, handler: nil)
        
        
        // relate actions to controllers
        //otherAlert.addAction(printSomething)
        
        
        otherAlert.addAction(dismiss)
        
        present(otherAlert, animated: true, completion: nil)
        reiniciar();
        
    }
    
    func reiniciar(){
        self.geo = ""
        self.contadorDetenidos = 0
        self.contadorMovimiento  = 0
        self.contadorFalla  = 0
        self.contadorPanico  = 0
        self.contadorApagado  = 0
        self.contadorReposo  = 0
        self.contadorEncendido  = 0
        self.contadorEntrada  = 0
        self.contadorSalida  = 0
        self.contadorTransito  = 0
        self.contadorTotal  = 0
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        //zoomToAllButtonPressed() no sirve de nada
        super.viewWillAppear(animated)
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        //   startTimer ()
        // update the map when we're told that a Position has been updated
        
        // actualizar el mapa cuando nos dicen que se ha actualizado una Posición
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(MapViewController.refreshPositions),
                                               name: NSNotification.Name(rawValue: Definitions.PositionUpdateNotificationName),
                                               object: nil)
        
        // reload Devices and Positions when the user logs in, show login screen when user logs out
        
        // recarga dispositivos y posiciones cuando el usuario inicia sesión, muestra la pantalla de inicio de sesión cuando el usuario cierra la sesión
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(MapViewController.loginStatusChanged),
                                               name: NSNotification.Name(rawValue: Definitions.LoginStatusChangedNotificationName),
                                               object: nil)
        
        // we need to fire this manually when the view appears
        loginStatusChanged()
    }
    
    
    func myHandler(alert: UIAlertAction){
        let d = UserDefaults.standard
        // d.setValue(self.serverField!.text!, forKey: TCDefaultsServerKey)
        
        d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
        stopTimer();
        alarma = false;
        User.sharedInstance.logout()
          mapView?.mapType = MKMapType.standard;
        mapView?.removeAnnotations((mapView?.annotations)!)
        
        WebService.sharedInstance.disconnectWebSocket()
        shouldCenterOnAppear = true
        count = 0
        self.ubication = false
        
        
    }
    

    enum maptype:NSInteger
    {
        case standardmap = 0
        case satellitemap = 1
        case hybridmap = 2
    }
    
    @IBAction func maptypes(_ sender: Any)
    {
        switch(sender as AnyObject).selectedSegmentIndex
        {
        case maptype.standardmap.rawValue:
            mapView?.mapType = .standard
        case maptype.satellitemap.rawValue:
            mapView?.mapType = .satellite
        case maptype.hybridmap.rawValue:
            mapView?.mapType = .hybrid
        default:
            break
        }
    }
    @IBAction func logoutButtonPressed() {
        
        
        let otherAlert = UIAlertController(title: "Realmente Desea Salir?", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        /* let printSomething = UIAlertAction(title: "Print", style: UIAlertActionStyle.default) { _ in
         print("We can run a block of code." )
         }
         */
        let callFunction = UIAlertAction(title: "Salir", style: UIAlertActionStyle.destructive, handler: myHandler)
        
        let dismiss = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil)
        
     
        otherAlert.addAction(callFunction)
        otherAlert.addAction(dismiss)
        
        present(otherAlert, animated: true, completion: nil)
        
        
        
        
    }
    // boton que muestra los moviles
    @IBAction func devicesButtonPressed() {
        performSegue(withIdentifier: "ShowDevices", sender: self)
    }
    
    
    @IBAction func rutasButtonPressed() {
        performSegue(withIdentifier: "rutas", sender: self)
    }
    
    @IBAction func confiButton() {
        changeMap()
        
    }
    
    
    @IBAction func zoomToAllButtonPressed() {
    
        mapView?.removeAnnotations((mapView?.annotations)!)
        
        
        WebService.sharedInstance.reconnectWebSocket2()
          WebService.sharedInstance.fetchDevices(onSuccess: { (newDevices) in
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.devices = newDevices
            self.refreshPositions()
    
            
            
        })
        
    }
    
  
    
    func loginStatusChanged() {
        
        if User.sharedInstance.isAuthenticated {
            //            if shouldCenterOnAppear {
            //                let centerCoordinates = User.sharedInstance.mapCenter
            //                assert(CLLocationCoordinate2DIsValid(centerCoordinates), "Map center coordinates aren't valid")
            //                self.mapView?.setCenter(centerCoordinates, animated: true)
            //
            //                shouldCenterOnAppear = false
            //            }
            
            WebService.sharedInstance.fetchDevices(onSuccess: { (newDevices) in
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                self.devices = newDevices
                
                // if devices are added/removed from the server while user is logged-in, the
                // positions will be added/removed from the map here
                self.refreshPositions()
                self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: false)
                
            })
            
        } else {
        performSegue(withIdentifier: "ShowLogin", sender: self)
        }
        
        //performSegue(withIdentifier: "ShowLogin", sender: self)
        
        
    }
    
    
    
    
    
    func refreshPositions() {
      
      /*  if(self.ubication == true){
             determineCurrentLocation()
        }
     */
        mapView?.removeAnnotations((mapView?.annotations)!)
        
        // positions of devices
        self.positions = WebService.sharedInstance.positions
        
        for device in self.devices {
            
            var annotationForDevice: PositionAnnotation?
            for existingAnnotation in (self.mapView?.annotations)! {
                if let a = existingAnnotation as? PositionAnnotation {
                    if a.deviceId == device.id {
                        annotationForDevice = a
                        
                        break
                    }
                }
            }
            
            if let a = annotationForDevice {
                
                if let p = WebService.sharedInstance.positionByDeviceId(a.deviceId!) {
                    if p.id == a.positionId {
                        // this annotation is still current, don't change it
                    } else {
                        a.coordinate = p.coordinate
                        a.title = p.annotationTitle
                        a.subtitle = p.estadoString
                        
                      //  if(p.geozoneID != nil || p.geozoneID != "")
                      //  {
                           // self.geo = self.geo! + "EL móvil " + a.title! + " Esta En" + p.geozoneID! + "\r\n"
                     
                        
                            
                      //  }
                       // a.velocidad = p.speedString
                        a.positionId = p.id
                        if(p.status == 61715)
                        {
                            self.color = .red
                            
                        }else if (p.status == 61724){
                            
                            
                            self.color = .green
                        }
                            
                        else{
                            self.color = .red
                        }
                        
                        // device ID will not have changed
                    }
                } else {
                    // the position for this device has been removed
                    self.mapView?.removeAnnotation(a)
                }
                
            } else {
                
                // there's no annotation for the device's position, we need to add one
                
                if let p = WebService.sharedInstance.positionByDeviceId(device.id!) {
                    let a = PositionAnnotation()
                    a.coordinate = p.coordinate
                    a.title = p.annotationTitle
                    a.subtitle = p.estadoString
                   
                   
                  
           
                   
                
                   /*
                    if(p.estadoString?.contains("En Movimiento"))!
                    {
                        self.scheduleNotification(tile: device.name!, sub: p.estadoString!);
                    }
                    */
                    a.positionId = p.id
                    a.deviceId = p.deviceId
                    if(p.status == 61715)
                    {
                        self.color = .red
                        
                    }else if (p.status == 61724){
                        
                        
                        self.color = .green
                    }
                        
                    else{
                          self.color = .red
                    }
                    
                    self.mapView?.addAnnotation(a)
                    
                    //self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: false)
                }
                
            }
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
       
        
        
        
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            pinView!.canShowCallout = true
            
            
            //prueba
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            let lbl = UILabel(frame: CGRect(x: -25, y: -10, width: 50, height: 20))
           
                lbl.textColor = .black
                
              // lbl.alpha = 0.5
                lbl.tag = 100
                lbl.adjustsFontSizeToFitWidth = true
                lbl.backgroundColor = self.color
                
                //lbl.font = UIFont.init(name: "HelveticaBold", size: 15)
                pinView?.addSubview(lbl)
                pinView?.canShowCallout = true
                pinView?.isSelected = true
                pinView?.isHighlighted = true
         
          
            
                
                
            
                
            
            
        }
        let btn = UIButton(type: .detailDisclosure)
        btn.addTarget(self, action: #selector(MapViewController.didTapMapPinDisclosureButton), for: UIControlEvents.touchUpInside)
        pinView?.rightCalloutAccessoryView = btn
        let lbl = pinView?.viewWithTag(100) as! UILabel
        
        if(annotation.subtitle! == "En Movimiento")
        {
            
            lbl.backgroundColor = .green
        }
        else if(annotation.subtitle! == "Detenido")
            
        {
            lbl.backgroundColor = .red
            
        }
        else if(annotation.subtitle! == "Entrada A Geozona" || annotation.subtitle! == "Salida De Geozona" )
            
        {
            lbl.backgroundColor = .orange
            
        }
        else{
            lbl.backgroundColor = .red
        }
       
        if(annotation.subtitle! == "En Movimiento")
        {
            
        }
        if(self.ubication == true)
        {
           self.mapView?.showsUserLocation = true;
            
            
          
            
        }
        if(self.ubication == false)
        {
            self.mapView?.showsUserLocation = false;
            
   
            
        }
        
        lbl.text = annotation.title!
        
       
         pinView!.annotation = annotation
        
        return pinView
    }
    
    // MARK: handle the tap of a map pin info button
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let a = view.annotation as? PositionAnnotation {
            selectedAnnotation = a
        }
    }
    
    func didTapMapPinDisclosureButton(_ sender: UIButton) {
        if selectedAnnotation != nil {
            performSegue(withIdentifier: "ShowDeviceInfo", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // running on iPhone
        if let dvc = segue.destination as? DeviceInfoViewController {
            
            // establecer el dispositivo en la vista de información
            if let deviceId = selectedAnnotation?.deviceId {
                if let device = WebService.sharedInstance.deviceById(deviceId) {
                    dvc.device = device
                }
            }
            
        } else if let nc = segue.destination as? UINavigationController {
            
            if let dvc = nc.topViewController as? DeviceInfoViewController {
                
                // set device on the info view
                if let deviceId = selectedAnnotation?.deviceId {
                    if let device = WebService.sharedInstance.deviceById(deviceId) {
                        dvc.device = device
                    }
                }
                
                // show a close button if we're running on an iPad
                dvc.shouldShowCloseButton = true
            }
            
        }
    }
    
}


