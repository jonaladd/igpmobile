//
// Copyright 2016 William Pearse (w.pearse@gmail.com)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import CoreLocation

/*
 
 TODO: Position attributes?
 
    {
     "status": "00000000",
     "charge": true,
     "ignition": false,
     "odometer": 0,
     "ip": "118.148.169.48"
    }
 
*/

class Position: NSObject {
    
    var id: NSNumber?
    var deviceId: NSNumber?
    var deviceId2: String?
     var accountID: String?
     var userID: String?
    var attributes: [String : AnyObject]?
    
    // protocol is reserved
    var positionProtocol: String?
    var timestampGeozone: String?
      var status: NSNumber?
     var alarma: NSNumber?
    // type is reserved
    var positionType: String?

    var mensaje: String?
    var geozoneID: String?
     var geozoneIDa: String?
    
    var latitude: NSNumber?
    var longitude: NSNumber?
    var speed: NSNumber?
    var course: NSNumber?
    var address: String?
    var altitude: NSNumber?
   
    // outdated is reserved
    var isOutdated: Bool?
    
    // valid is reserved
    var isValid: Bool?
    
    var serverTime: Date?
    var deviceTime: Date?
    var fixTime: String?
    
    var device: Device? {
        get {
            return WebService.sharedInstance.deviceById(deviceId!)
        }
    }
    

    // scripts para interpretar el curso de la direccion
    // http://stackoverflow.com/a/13220694/336419
    static let compassDirections = ["NORTE", "NNE", "NE", "ENE", "ESTE", "ESE", "SE", "SSE",
    "SUR", "SSW", "SW", "WSW", "OESTE", "WNW", "NW", "NNW"]
    
    // returns the course of this position as a compass direction
    var courseDirectionString : String? {
        get {
            if let c = course {
                let idx = ((c.doubleValue + 11.25) / 22.5).truncatingRemainder(dividingBy: 16)
                return "\(Position.compassDirections[Int(idx)]), \(c.intValue) º"
            }
            return nil
        }
    }
    
    // funcion creada para obtener el estado de la posicion
    var estadoString : String? {
        get {
            var estado: String?
            if let c = status {
                
                switch c {
                    
                case 61715:
                    
                    
                    estado = "Detenido"
                
                    
                    break
                case 0:
                    
                    
                    estado = "Detenido"
                    
                    
                    break
                case 61716:
                    
                    
                    estado = "En reposo"
                   
                    
                    break
                case 61724:
                    
                    
                    estado = "En Movimiento"
                    
                    
                    break
                case 62467:
                    
                    
                    estado = "Motor Apagado"
                    
                    
                    break
                case 64787:
                    
                    
                    estado = "Falla De Energía"
            
                    
                    break
                case 62497:
                    
                    
                    estado = "Boton De Panico"
                    
                    
                    break
                case 61968:
                    
                    
                    estado = "Entrada A Geozona"
                    
                    
                    break
                case 62000:
                    
                    
                    estado = "Salida De Geozona"
                    
                    
                    break
                case 65399:
                    
                    
                    estado = "Descarga"
                    
                    
                    break
                case 62548:
                    
                    
                    estado = "Cierre De Puertas"
                    
                    
                    break
                case 62516:
                    
                    
                    estado = "Apertura De Puertas"
                    
                    
                    break
                case 65300:
                    
                    
                    estado = "Boton Cancelar"
                    
                    
                    break
                case 62001:
                    
                    
                    estado = "Transito Entre Geozonas"
                    
                    
                    break
                case 62465:
                    
                    
                    estado = "Motor encendido"
                    
                    
                    break
              
                default :
                    
                  
                    
                    break
                }
                
                
                
                
                
                
                return estado
            }
            return nil
        }
    }
    
    
    
    
    var alarmaString : String? {
        get {
            var estado: String?
            if let c = alarma {
                
                switch c {
                    
                case 61716:
                    
                    
                    estado = "En Reposo"
                    
                    
                    break
                case 61720:
                    
                    
                    estado = "Exceso de Reposo"
                    
                    
                    break
                case 61968:
                    
                    
                    estado = "Entrada en Geozona"
                    
                    
                    break
                case 62000:
                    
                    
                    estado = "Salida de Geozona"
                    
                    
                    break
                case 62001:
                    
                    
                    estado = "Transito en Geozonas"
                    
                    
                    break
                case 62002:
                    
                    
                    estado = "Tiempo en Geozonas"
                    
                    
                    break
                case 62465:
                    
                    
                    estado = "Motor Encendido"
                    
                    
                    break
                case 62467:
                    
                    
                    estado = "Motor Apagado"
                    
                    
                    break
                case 62497:
                    
                    
                    estado = "Boton de Panico"
                    
                    
                    break
                case 62516:
                    
                    
                    estado = "Apertura de Puertas"
                    
                    
                    break
                case 62548:
                    
                    
                    estado = "Cierre de Puertas"
                    
                    
                    break
                case 64787:
                    
                    
                    estado = "Falla de Energia"
                    
                    
                    break
                case 64834:
                    
                    
                    estado = "Velocidad"
                    
                    
                    break
                case 64835:
                    
                    
                    estado = "Velocidad maxima en Geozona"
                    
                    
                    break
                
                    
                default :
                    
                    
                    
                    break
                }
                
                
                
                
                
                
                return estado
            }
            return nil
        }
    }
    var speedString: String? {
        get {
            if let s = speed {
                
                // format speed to 1 dp
                let speedFormatter = NumberFormatter()
                speedFormatter.numberStyle = .decimal
                speedFormatter.maximumFractionDigits = 1
                let formattedSpeed = speedFormatter.string(from: s)
                if let fs = formattedSpeed {
                    if let u = User.sharedInstance.speedUnit {
                        return "\(fs) \(u)"
                    } else {
                        return fs
                    }
                }
            }
            return nil
        }
    }
    
    // used to format the latitude and longitudes to 5 dp (fixed),
    // this gives about 10cm resolution and is plenty
    fileprivate var latLonFormatter: NumberFormatter
    
    var latitudeString: String? {
        get {
            if let l = latitude {
                return latLonFormatter.string(from: l)
            }
            return nil
        }
    }
    
    var longitudeString: String? {
        get {
            if let l = longitude {
                return latLonFormatter.string(from: l)
            }
            return nil
        }
    }
    
    var statusString: String? {
        get {
            if let l = status {
                return latLonFormatter.string(from: l)
            }
            return nil
        }
    }
    var coordinate: CLLocationCoordinate2D {
        guard let lat = latitude else {
            return kCLLocationCoordinate2DInvalid
        }
        guard let lon = longitude else {
            return kCLLocationCoordinate2DInvalid
        }
        return CLLocationCoordinate2DMake(lat.doubleValue, lon.doubleValue)
    }
    
    var annotationTitle: String {
        get {
            if let d = device {
                return d.name!
            }
            return "Device \(deviceId!.int32Value)"
        }
    }
    
    var annotationSubtitle: String {
        get {
            if let a = address {
                return a as String
            }
            return ""
        }
    }
    
    override init() {
        self.latLonFormatter = NumberFormatter()
        self.latLonFormatter.numberStyle = .decimal
        self.latLonFormatter.minimumFractionDigits = 5
        
        super.init()
    }
    
    // implemented so we don't crash if the model changes
    override func setValue(_ value: Any?, forUndefinedKey key: String) {
        print("Tried to set property '\(key)' that doesn't exist on the Position model")
    }
    
    override func value(forUndefinedKey key: String) -> Any? {
        return nil
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if key == "protocol" {
            self.positionProtocol = value as? String
        } else if key == "type" {
            self.positionType = value as? String
        } else if key == "valid" {
            self.isValid = value as? Bool
        } else if key == "outdated" {
            self.isOutdated = value as? Bool
        } else if key == "serverTime" {
            if let v = value as? String {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"
                self.serverTime = dateFormatter.date(from: v)
            }
        } else if key == "deviceTime" {
            if let v = value as? String {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"
            
              

                self.deviceTime = dateFormatter.date(from: v)
            }
        }  else {
            super.setValue(value, forKey: key)
        }
    }
    
}
