

import Foundation
import CoreLocation

class User: NSObject {
    static let sharedInstance = User()
    var admin: NSNumber?
    var distanceUnit: String?
    var email: String?
    var id: NSNumber?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var map: String?
    var name: String?
    var readonly: NSNumber?
    var speedUnit: String?
    var twelveHourFormat: NSNumber?
    var zoom: NSNumber?
    var mapCenter: CLLocationCoordinate2D {
        guard let lat = latitude else {
            return kCLLocationCoordinate2DInvalid
        }
        guard let lon = longitude else {
            return kCLLocationCoordinate2DInvalid
        }
        return CLLocationCoordinate2DMake(lat.doubleValue, lon.doubleValue)
    }
    
    var isAuthenticated: Bool {
        // TODO: this really isn't a good test
        return email != nil
    }
    
    func logout() {
        
        WebService.sharedInstance.disconnectWebSocket()
        email = nil
      //  HTTPCookieStorage.shared.removeCookies(since: Date(timeIntervalSinceReferenceDate: 0))
           let d = UserDefaults.standard
           d.removeObject(forKey: "DefaultsAccountKey")
           d.removeObject(forKey: "DefaultsEmailKey")
           d.removeObject(forKey: "DefaultsPasswordKey")
           d.removeObject(forKey: "DefaultsAlertKey")
           d.removeObject(forKey: "DefaultsMapsKey")
            d.removeObject(forKey: "DefaultsGpsKey")
           d.synchronize()
        // tell everyone that the user has logged out
        NotificationCenter.default.post(name: Notification.Name(rawValue: Definitions.LoginStatusChangedNotificationName), object: nil)
    }
    
    override func setValue(_ value: Any?, forUndefinedKey key: String) {
        // ignore the password, we don't care about it
        if key != "password" {
            print("Tried to set property '\(key)' that doesn't exist on the User model")
        }
    }
    
}
