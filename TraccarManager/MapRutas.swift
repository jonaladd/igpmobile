//
//  MapRutas.swift
//  com.igpsmobile
//
//  Created by Jonathan Leon on 17-05-18.
//  Copyright © 2018 i-gps.com. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SocketIO
import SocketRocket
import Foundation

class MapRutas: UIViewController, MKMapViewDelegate {
    
    
    let TCDefaultsAccountKey = "DefaultsAccountKey"
    let TCDefaultsEmailKey = "DefaultsEmailKey"
    let TCDefaultsPasswordKey = "DefaultsPasswordKey"
    let TCDefaultsAlertKey = "DefaultsAlertKey"
    let TCDefaultsMapsKey = "DefaultsMapsKey"
    var textField = UITextField()
    var datePicker = UIDatePicker()
    var datePicker2 = UIDatePicker()
    var mapa: String?
    var mapa2: String?
    var toolBar = UIToolbar()
    var toolBar2 = UIToolbar()
    var doneButton = UIButton()
    var doneButton2 = UIButton()
    var device: Device?
    var serverURL2: String?
    var account: String?
    var direccion : String?
    var email: String?
    var password: String?
    var velocidad: String?
    var dispositivo: String?
    var fechaInicial2: String?
    var fechaFinal2: String?
    var fechaInicial3: String?
    var fechaFinal3: String?
    var devici: String?
    var status: NSNumber?
    var estado: NSNumber? = 0;
    var estado2: NSNumber? = 0;
    var dato: String?
    var flag = false;
    var speed: Double?;
    var imagen = UIImage()
    var devices: [Device]?
    var posi: [Position]?
   
    var date: NSDate?

  
    @IBOutlet var mapView: MKMapView?
    fileprivate var allDevices: NSMutableDictionary = NSMutableDictionary()
    fileprivate var allPositions: NSMutableDictionary = NSMutableDictionary()
    fileprivate var positions: [Position] = []
    private var positions2: [Position] {
        get {
            return allPositions.allValues as! [Position]
        }
    }
    
    // mapa cada vez que aparece esta vista
    fileprivate var shouldCenterOnAppear: Bool = true
    var selectedAnnotation: PositionAnnotation?
   
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let d = UserDefaults.standard
        if let e = d.string(forKey: TCDefaultsMapsKey ) {
            mapa = e
            
            
        }
        if(mapa != nil)
        {
            if(mapa == "MKMapType.standard"){
                mapView?.mapType = MKMapType.standard;
            }
            if(mapa == "MKMapType.satellite"){
                mapView?.mapType = MKMapType.satellite;
            }
            if(mapa == "MKMapType.hybrid"){
                mapView?.mapType = MKMapType.hybrid;
            }
        }
        else{
            mapView?.mapType = MKMapType.standard;
            let d = UserDefaults.standard
            
            d.setValue("MKMapType.standard", forKey: TCDefaultsMapsKey)
            // d.setValue("no", forKey: TCDefaultsAlertKey)
            d.synchronize()
        }
  
    
       //   self.mapView?.delegate = self
       // mapView?.mapType = MKMapType.standard;
        mapView?.showsPointsOfInterest = true;
       // mapView?.showsTraffic = true;
        mapView?.showsCompass = true;
        mapView?.isPitchEnabled = true;
        mapView?.isZoomEnabled = true;
        mapView?.isScrollEnabled = true;
        let p = dispositivo
     

      // WebService.sharedInstance.reconnect()
 
        zoomToAllButtonPressed2();
      
       dos();
    
        
        
        // don't let user open devices view until the devices have been loaded
        
        // no permita que el usuario abra dispositivos vista hasta que los dispositivos hayan sido cargados
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func satelite(alert: UIAlertAction){
        mapView?.mapType = MKMapType.satellite;
        let d = UserDefaults.standard
        
        d.setValue("MKMapType.satellite", forKey: TCDefaultsMapsKey)
        // d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
        
         self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: true)
    }
    func standar(alert: UIAlertAction){
        mapView?.mapType = MKMapType.standard;
        let d = UserDefaults.standard
        
        d.setValue("MKMapType.standard", forKey: TCDefaultsMapsKey)
        // d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
         self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: true)
        
    }
    func hybrid(alert: UIAlertAction){
        mapView?.mapType = MKMapType.hybrid;
        let d = UserDefaults.standard
        
        d.setValue("MKMapType.hybrid", forKey: TCDefaultsMapsKey)
        // d.setValue("no", forKey: TCDefaultsAlertKey)
        d.synchronize()
         self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: true)
        
    }
    func changeMap2(alert: UIAlertAction){
        let otherAlert = UIAlertController(title: "Visualizaciones Del Mapa", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        let d = UserDefaults.standard
        if let e = d.string(forKey: TCDefaultsMapsKey ) {
            mapa2 = e
            
            
        }
        /* let printSomething = UIAlertAction(title: "Print", style: UIAlertActionStyle.default) { _ in
         print("We can run a block of code." )
         }
         */
        if(mapa2 != nil)
        {
            if(mapa2 == "MKMapType.standard"){
                let callstandar = UIAlertAction(title: "Mapa Estandar              ✔", style: UIAlertActionStyle.default, handler: standar)
                
                let dismiss = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.destructive, handler: nil)
                let callhybrid  = UIAlertAction(title: "Mapa híbrido                 ", style: UIAlertActionStyle.default, handler: hybrid)
                let callhybrid2 = UIAlertAction(title: "Mapa Satelital               ", style: UIAlertActionStyle.default, handler: satelite)
                // relate actions to controllers
                //otherAlert.addAction(printSomething)
                otherAlert.addAction(callstandar)
                // otherAlert.addAction(callSatelite)
                otherAlert.addAction(callhybrid)
                otherAlert.addAction(callhybrid2)
                otherAlert.addAction(dismiss)
                
                present(otherAlert, animated: true, completion: nil)
            }
            if(mapa2 == "MKMapType.satellite"){
                let callstandar = UIAlertAction(title: "Mapa Estandar               ", style: UIAlertActionStyle.default, handler: standar)
                
                let dismiss = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.destructive, handler: nil)
                let callhybrid  = UIAlertAction(title: "Mapa híbrido                ", style: UIAlertActionStyle.default, handler: hybrid)
                let callhybrid2 = UIAlertAction(title: "Mapa Satelital             ✔", style: UIAlertActionStyle.default, handler: satelite)
                // relate actions to controllers
                //otherAlert.addAction(printSomething)
                otherAlert.addAction(callstandar)
                // otherAlert.addAction(callSatelite)
                otherAlert.addAction(callhybrid)
                otherAlert.addAction(callhybrid2)
                otherAlert.addAction(dismiss)
                
                present(otherAlert, animated: true, completion: nil)
            }
            if(mapa2 == "MKMapType.hybrid"){
                let callstandar = UIAlertAction(title: "Mapa Estandar              ", style: UIAlertActionStyle.default, handler: standar)
                
                let dismiss = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.destructive, handler: nil)
                let callhybrid  = UIAlertAction(title: "Mapa híbrido             ✔", style: UIAlertActionStyle.default, handler: hybrid)
                let callhybrid2 = UIAlertAction(title: "Mapa Satelital             ", style: UIAlertActionStyle.default, handler: satelite)
                // relate actions to controllers
                //otherAlert.addAction(printSomething)
                otherAlert.addAction(callstandar)
                // otherAlert.addAction(callSatelite)
                otherAlert.addAction(callhybrid)
                otherAlert.addAction(callhybrid2)
                otherAlert.addAction(dismiss)
                
                present(otherAlert, animated: true, completion: nil)
            }
        }
        else{
           
        }
       
      //  dos()
        
    }
    func changeMap(){
        
        let otherAlert = UIAlertController(title: "ELIGA UNA OPCIÓN", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        /* let printSomething = UIAlertAction(title: "Print", style: UIAlertActionStyle.default) { _ in
         print("We can run a block of code." )
         }
         */
        let callstandar = UIAlertAction(title: "Cambiar Mapa", style: UIAlertActionStyle.default, handler: changeMap2)
        
          let callhybrid2 = UIAlertAction(title: "Leyenda De Pines", style: UIAlertActionStyle.default, handler: info)
        
        let dismiss = UIAlertAction(title: "Cerrar", style: UIAlertActionStyle.destructive, handler: nil)
        
      
        
        otherAlert.addAction(callstandar)
        otherAlert.addAction(callhybrid2)
        otherAlert.addAction(dismiss)
        
        present(otherAlert, animated: true, completion: nil)
      
        
    }
  
    func info(alert: UIAlertAction){
        
        let alert = UIAlertController(title:"Leyenda De Pines", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let saveAction = UIAlertAction(title: "Zip Viewer", style: .default, handler: nil)
        
        // image set in alret view inside
        // size to fit in view
        // var image = UIImage(named: "logo")
        
        //image = image?.withAlignmentRectInsets(UIEdgeInsets(top: 0, left:  -40, bottom: 0, right: 50))
        
        
        saveAction.setValue(UIImage(named: "pines")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
        alert.addAction(saveAction)
        
        
        alert.addAction(UIAlertAction(title: "Cerrar", style: UIAlertActionStyle.default, handler: { alertAction in
            alert.dismiss(animated: true, completion: nil)
        }))
        
      
        
        self.present(alert, animated: true, completion: nil)
        
    }

    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        //zoomToAllButtonPressed() no sirve de nada
        super.viewWillAppear(animated)
   
   
    
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //   startTimer ()
        // update the map when we're told that a Position has been updated
        
        // actualizar el mapa cuando nos dicen que se ha actualizado una Posición
       /* NotificationCenter.default.addObserver(self,
                                               selector: #selector(MapRutas.refreshPositions2),
                                               name: NSNotification.Name(rawValue: Definitions.PositionUpdateNotificationName),
                                               object: nil)
        */
        // reload Devices and Positions when the user logs in, show login screen when user logs out
        
        // recarga dispositivos y posiciones cuando el usuario inicia sesión, muestra la pantalla de inicio de sesión cuando el usuario cierra la sesión
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(MapRutas.loginStatusChanged),
                                               name: NSNotification.Name(rawValue: Definitions.LoginStatusChangedNotificationName),
                                               object: nil)
        
        // we need to fire this manually when the view appears
        loginStatusChanged()
    }
    
    
    func myHandler(alert: UIAlertAction){
        
        User.sharedInstance.logout()
        //  socket.disconnect();
            mapView?.mapType = MKMapType.standard;
        mapView?.removeAnnotations((mapView?.annotations)!)
        mapView?.removeOverlays((mapView?.overlays)!)
        
        shouldCenterOnAppear = true
        
    }
    
    
    enum maptype:NSInteger
    {
        case standardmap = 0
        case satellitemap = 1
        case hybridmap = 2
    }
    
    @IBAction func maptypes(_ sender: Any)
    {
        switch(sender as AnyObject).selectedSegmentIndex
        {
        case maptype.standardmap.rawValue:
            mapView?.mapType = .standard
        case maptype.satellitemap.rawValue:
            mapView?.mapType = .satellite
        case maptype.hybridmap.rawValue:
            mapView?.mapType = .hybrid
        default:
            break
        }
    }
    @IBAction func logoutButtonPressed() {
        
        
       
        let otherAlert = UIAlertController(title: "Realmente Desea Salir?", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        let callFunction = UIAlertAction(title: "Salir", style: UIAlertActionStyle.destructive, handler: myHandler)
        let dismiss = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil)
        otherAlert.addAction(callFunction)
        otherAlert.addAction(dismiss)
        present(otherAlert, animated: true, completion: nil)
      //  mapView?.removeAnnotations((mapView?.annotations)!)
      //  mapView?.removeOverlays((mapView?.overlays)!)
    }
    @IBAction func Home() {
        performSegue(withIdentifier: "Home", sender: self)
    }
    @IBAction func Confi() {
        changeMap()
    }
    
   

    
    @IBAction func fechaInicial() {
        
        
        
        toolBar2 = UIToolbar(frame: CGRect(x: 0, y: 130, width: view.frame.width, height: 40))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width/3, height: 40))
        label.text = "Fecha Inicial"
        let labelButton = UIBarButtonItem(customView:label)
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    
        // Position date picket within a view
        datePicker.frame = CGRect(x: 0, y: 170, width: self.view.frame.width, height: 150)
        datePicker.timeZone = NSTimeZone.local
        datePicker.backgroundColor = UIColor(white: 1, alpha: 1)
        datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        datePicker.locale = NSLocale(localeIdentifier: "es_GB") as Locale //
        datePicker.addTarget(self, action: #selector(MapRutas.datePickerValueChanged(_:)), for: .valueChanged)
        let doneButton = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(MapRutas.doneButton(_:)))
        toolBar2.setItems([flexibleSpace,labelButton,flexibleSpace,doneButton], animated: true)
        self.view.addSubview(toolBar2)
           //self.view.addSubview(doneButton)
        // Add DataPicker to the view
        self.view.addSubview(datePicker)
   
        if (self.estado == 1)
        {
            datePicker.isHidden = false
            toolBar2.isHidden = false
            //doneButton.isHidden = false
            self.estado = 0
        }
        
    }
    @objc func todayButtonPressed(sender: UIBarButtonItem) {
       
     datePicker.datePickerMode = UIDatePickerMode.dateAndTime
      
    }
    
    func doneButton(_ sender: UIButton)
    {
           datePicker.isHidden = true
           toolBar2.isHidden = true
           //doneButton.isHidden = true
        
        self.estado = 1
        
     
    }
    
    
    
    func datePickerValueChanged(_ sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        let dateFormatter2: DateFormatter = DateFormatter()
        // Set date format
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        dateFormatter2.dateFormat = "yyyy/MM/dd/HH:mm:ss"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: sender.date)
        let selectedDate2: String = dateFormatter2.string(from: sender.date)
        self.fechaInicial2 = selectedDate
        self.fechaInicial3 = selectedDate2
       // print("Esta es la fecha inicial \(fechaInicial2)")
        
        
    }
    
    @IBAction func fechaFinal() {
        
        
        
        toolBar = UIToolbar(frame: CGRect(x: 0, y: 130, width: view.frame.width, height: 40))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width/3, height: 40))
        label.text = "fecha Final"
        let labelButton = UIBarButtonItem(customView:label)
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        
        // Posiiton date picket within a view
        datePicker2.frame = CGRect(x: 0, y: 170, width: self.view.frame.width, height: 150)
        datePicker2.timeZone = NSTimeZone.local
        datePicker2.backgroundColor = UIColor.white
        datePicker2.datePickerMode = UIDatePickerMode.dateAndTime
        datePicker2.locale = NSLocale(localeIdentifier: "es_GB") as Locale //

        datePicker2.addTarget(self, action: #selector(MapRutas.datePickerValueChanged2(_:)), for: .valueChanged)
        let doneButton = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(MapRutas.doneButton2(_:)))
        toolBar.setItems([flexibleSpace,labelButton,flexibleSpace,doneButton], animated: true)
        self.view.addSubview(toolBar)
        // Add DataPicker to the view
        self.view.addSubview(datePicker2)
        
        if (self.estado2 == 1)
        {
            datePicker2.isHidden = false
            doneButton2.isHidden = false
            toolBar.isHidden = false
            self.estado2 = 0
        }
        
    }
    func doneButton2(_ sender: UIButton)
    {
        datePicker2.isHidden = true
        doneButton2.isHidden = true
        toolBar.isHidden = true
        self.estado2 = 1
    }
    
    
    
    func datePickerValueChanged2(_ sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        let dateFormatter2: DateFormatter = DateFormatter()
        // Set date format
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        dateFormatter2.dateFormat = "yyyy/MM/dd/HH:mm:ss"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: sender.date)
        let selectedDate2: String = dateFormatter2.string(from: sender.date)
        self.fechaFinal2 = selectedDate
        self.fechaFinal3 = selectedDate2
       // print("Esta es la fecha final \(fechaFinal2)")
        
        
    }

    
    @IBAction func buscar() {
        
       if(self.fechaInicial2 == nil || self.fechaFinal2 == nil )
       {
        
        let ac = UIAlertController(title: "IMPORTANTE", message: "Debe ingresar el intervalo de fechas", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        ac.addAction(okAction)
        self.present(ac, animated: true, completion: nil)
        }
        mapView?.removeAnnotations((mapView?.annotations)!)
        mapView?.removeOverlays((mapView?.overlays)!)
        dos();
        
       // self.mapView?.showsUserLocation = true;
       // self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: true)
       
   
       // print("asi llegan las fecha  \(self.fechaInicial2)")
       // print("asi nomas con esto \(self.fechaFinal2)")
        
    }
    
    
    @IBAction func rutasButtonPressed() {
        performSegue(withIdentifier: "rutas", sender: self)
    }
    
    
    @IBAction func zoomToAllButtonPressed2() {
        
        
        
        mapView?.removeAnnotations((mapView?.annotations)!)
        
            dos();
            WebService.sharedInstance.fetchDevices(onSuccess: { (newDevices) in
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.devices = newDevices
 
        })
        
    }
 
    func dos(_ onFailure: ((String) -> Void)? = nil){
        var annotations = [MKPointAnnotation]()
       var contador: NSNumber? = 0;
        var estado2: String?
        let dd = Position()
          let kk = Position()
        let account = WebService.sharedInstance.account
        let email = WebService.sharedInstance.email
        let password = WebService.sharedInstance.password
       let dispo = dispositivo
       let dispo2 = devici
       // print("este es valor del dispositivo final " + dispo!)
       //  print("este es valor del dispositivo final " + dispo2!)
      //  print(account)
       // print(email)
       // print(password)
        
       
      //  let url = URL(string: "http://186.10.30.106:82/CDCmobile/ios/api/rutas2/?accountID=iwingenieria&userID=andres&password=q&device=72001&fechaInicial=20180505101034&fechaFinal=20180506101034")!
        if(self.fechaInicial2 == nil || self.fechaFinal2 == nil)
        {
            self.fechaInicial2 = ""
            self.fechaFinal2 = ""
            self.fechaInicial3 = ""
            self.fechaFinal3 = ""
        }else{
            self.flag = true;
        }
         /* URL PRINCIPAL  */
        let url2 = "http://site1.i-gps.cl/CDCmobile/ios/api/rutas/?accountID=" + account!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&userID=" + email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&password=" + password!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&device=" + dispo2!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&fechaInicial=" + self.fechaInicial2!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) + "&fechaFinal=" + self.fechaFinal2!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        Alamofire.request(url2).responseJSON(completionHandler: { response in
            switch response.result {
            case .success(let JSON):
                if response.response!.statusCode != 200 {
                    let ac = UIAlertController(title: "IMPORTANTE", message: "no hay conexion con el servidor", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    ac.addAction(okAction)
                    self.present(ac, animated: true, completion: nil)
                    if let fail = onFailure {
                        fail("Invalid server response")
                       // print(JSON);
                        // print("errrrrrrrrrrrrror server");
                        
                        
                        
                    }
                } else {
                   
                    
                    if let data2 = JSON as? [[String : AnyObject]] {
                        
                      
                     
                        if(data2.isEmpty)
                        {
                            
                            
                            
                           
                            
                            if(self.flag == false)
                            {
                                let ac = UIAlertController(title: "IMPORTANTE", message: "no hay rutas para las últimas 24 horas", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                ac.addAction(okAction)
                                self.present(ac, animated: true, completion: nil)
                            }else{
                                if(self.fechaInicial2 == "" && self.fechaFinal2 == "" )
                                {
                                    let ac = UIAlertController(title: "INFORMACIÓN", message: "no hay rutas para las últimas 24 horas", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                    ac.addAction(okAction)
                                    self.present(ac, animated: true, completion: nil)
                                }else{
                                    
                                    let ac = UIAlertController(title: "INFORMACIÓN", message: "no hay rutas para el intervalo del " + self.fechaInicial3! + "\t hasta \t " + self.fechaFinal3!, preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                    ac.addAction(okAction)
                                    self.present(ac, animated: true, completion: nil)
                                }
                               
                            }
                           
                        }
                        
                      
                        var positions = [Position]()
                         var posiciones = [Position]()
                        
                    
                        
                        for d in data2 {
                          
                           
                
                           
                           
                            
                          let annotation = ColorPointAnnotation()
                           // let annotation = MKAnnotation()
                            dd.setValuesForKeys(d)
                            positions.append(dd)
                             self.status = dd.status!
                            let speed2 = dd.speed as!  Int
                            self.posi?.append(dd)
                            print(speed2)
                            let curso = dd.course as! Float
                           
                            
                            if(dd.mensaje == "over")
                            {
                                let ac = UIAlertController(title: "IMPORTANTE", message: "el intervalo de fechas no debe superar las 24 horas", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                ac.addAction(okAction)
                                self.present(ac, animated: true, completion: nil)
                                
                            }
                            if(self.fechaInicial2 == "" && self.fechaFinal2 == "" )
                            {
                                let ac = UIAlertController(title: "INFORMACIÓN", message: "rutas para las últimas 24 horas", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                ac.addAction(okAction)
                                self.present(ac, animated: true, completion: nil)
                            }else{
                                
                                let ac = UIAlertController(title: "INFORMACIÓN", message: "rutas para el intervalo del " + self.fechaInicial3! + "\t hasta \t " + self.fechaFinal3!, preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                ac.addAction(okAction)
                                self.present(ac, animated: true, completion: nil)
                            }
                           
                            // annotation.subtitle = "direccion caca"
                          
                            
                            
                          //  let direccion2 = self.direccion
                           
                    
                          
                            if(speed2 >= 32)
                            {
                                
                                if(curso >= 22.5 && curso < 67.5){
                                   //  self.imagen =  UIImage(named: "pin30_green_h1")!;
                                    annotation.imagen =  UIImage(named: "pin30_green_h1")!;
                                    
                                    
                                }
                                if(curso >= 67.5 && curso < 112.5){
                                    
                                    //  self.imagen =  UIImage(named: "pin30_green_h2")!;
                                    annotation.imagen =  UIImage(named: "pin30_green_h2")!;
                                }
                                if(curso >= 112.5 && curso < 157.5){
                                    
                                     // self.imagen =  UIImage(named: "pin30_green_h3")!;
                                    annotation.imagen =  UIImage(named: "pin30_green_h3")!;
                                }
                                if(curso >= 157.5 && curso < 202.5){
                                    
                                     // self.imagen =  UIImage(named: "pin30_green_h4")!;
                                    annotation.imagen =  UIImage(named: "pin30_green_h4")!;
                                }
                                if(curso >= 202.5 && curso < 247.5){
                                    
                                     // self.imagen =  UIImage(named: "pin30_green_h5")!;
                                    annotation.imagen =  UIImage(named: "pin30_green_h5")!;
                                }
                                if(curso >= 247.5 && curso < 292.5){
                                    
                                     // self.imagen =  UIImage(named: "pin30_green_h6")!;
                                    annotation.imagen =  UIImage(named: "pin30_green_h6")!;
                                }
                                if(curso >= 292.5 && curso < 337.5){
                                     // self.imagen =  UIImage(named: "pin30_green_h7")!;
                                    annotation.imagen =  UIImage(named: "pin30_green_h7")!;
                                    
                                }
                                if(curso >= 337.5 || curso < 22.5){
                                    
                                     // self.imagen =  UIImage(named: "pin30_green_h0")!;
                                    annotation.imagen =  UIImage(named: "pin30_green_h0")!;
                                }
                                
                               
                                
                                
                            }
                            if(speed2 < 32 && speed2 >= 8)
                            {
                                if(curso >= 22.5 && curso < 67.5){
                                   // self.imagen =  UIImage(named: "pin30_yellow_h1")!;
                                    annotation.imagen = UIImage(named: "pin30_yellow_h1")!;
                                    
                                }
                                if(curso >= 67.5 && curso < 112.5){
                                    
                                   // self.imagen =  UIImage(named: "pin30_yellow_h2")!;
                                     annotation.imagen = UIImage(named: "pin30_yellow_h2")!;
                                }
                                if(curso >= 112.5 && curso < 157.5){
                                    
                                   // self.imagen =  UIImage(named: "pin30_yellow_h3")!;
                                     annotation.imagen = UIImage(named: "pin30_yellow_h3")!;
                                }
                                if(curso >= 157.5 && curso < 202.5){
                                    
                                   // self.imagen =  UIImage(named: "pin30_yellow_h4")!;
                                     annotation.imagen = UIImage(named: "pin30_yellow_h4")!;
                                }
                                if(curso >= 202.5 && curso < 247.5){
                                    
                                   // self.imagen =  UIImage(named: "pin30_yellow_h5")!;
                                     annotation.imagen = UIImage(named: "pin30_yellow_h5")!;
                                }
                                if(curso >= 247.5 && curso < 292.5){
                                    
                                   // self.imagen =  UIImage(named: "pin30_yellow_h6")!;
                                     annotation.imagen = UIImage(named: "pin30_yellow_h6")!;
                                }
                                if(curso >= 292.5 && curso < 337.5){
                                   // self.imagen =  UIImage(named: "pin30_yellow_h7")!;
                                     annotation.imagen = UIImage(named: "pin30_yellow_h7")!;
                                    
                                }
                                if(curso >= 337.5 || curso < 22.5){
                                    
                                   // self.imagen =  UIImage(named: "pin30_yellow_h0")!;
                                     annotation.imagen = UIImage(named: "pin30_yellow_h0")!;
                                }
                                
                            }
                            
                            if(speed2 < 8)
                            {
                                // self.imagen =  UIImage(named: "pin30_red_dot")!;
                                 annotation.imagen = UIImage(named: "pin30_red_dot")!;
                            }
                           
                            
                            if(dd.id?.intValue == data2.count - 1)
                            {
                     
                                 annotation.imagen = UIImage(named: "pin30_blue")!;
                               
                            
                            
                                
                               
                             
                               
                               
                            }
                            
                           
                            
                            
                            
                            if(dd.estadoString != nil){
                            
                         //   annotation.title = "#:" + (dd.address)!
                            annotation.title = dispo!
                           annotation.info = "#:\t"  + (dd.id?.stringValue)! + "\r\n"
                                + "Dirección:\t" + dd.address! + "\r\n"
                                + "fecha De Ubicación:\t" + dd.fixTime! + "\r\n"
                                + "estado:\t" + dd.estadoString! + "\r\n"
                                + "longitud:\t" + dd.longitudeString! + "\r\n"
                                + "lantitud:\t" + dd.latitudeString! + "\r\n"
                                + "velocidad:\t" + dd.speedString!  + "KM/H" + "\r\n"
                                + "curso:\t" + dd.courseDirectionString! + "\r\n"
                                + "altitud :\t" + (dd.altitude?.stringValue)! + "\t metros" + "\r\n"
                                + "geozona:\t" + dd.geozoneID! + "\r\n"
                                self.mapView?.updateConstraints();
                                
                                
                            }
                    
                           // self.subtitleView.text = self.dato
                           // print(dd.address)
                            if (dd.id == nil)
                            {
                                
                                let ac = UIAlertController(title: "IMPORTANTE", message: "no hay utas para el intervalo", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                ac.addAction(okAction)
                                self.present(ac, animated: true, completion: nil)
                            }
                
                            let location = CLLocationCoordinate2DMake(dd.latitude as! CLLocationDegrees, dd.longitude as! CLLocationDegrees )
                            annotation.coordinate = CLLocationCoordinate2D(latitude: dd.latitude as! CLLocationDegrees , longitude: dd.longitude as! CLLocationDegrees)
                            self.mapView?.addAnnotation(annotation)
                            let span = MKCoordinateSpanMake(50, 50)
                           // let locations = [CLLocationCoordinate2DMake(dd.latitude as! CLLocationDegrees, dd.longitude as! CLLocationDegrees)]
                            let region = MKCoordinateRegion(center: location , span: span)
                            self.mapView?.setRegion(region, animated: true)
                           // self.mapView?.showsUserLocation = true;
                            annotation.coordinate = location
                           // agrego los puntos
                            annotations.append(annotation)
                            let points = annotations.map { $0.coordinate }
                            let polyline = MKPolyline(coordinates: points, count: points.count)
                            self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: true)
                            self.mapView?.add(polyline)
                            annotation.coordinate = location
          
                        }
                        
                        
                      
                        
                        
                    } else {
                        
                     
                            
                        
                        
                        if let fail = onFailure {
                            fail("Server response was invalid")
                            print("mala mamamamamammamamamamam");
                            
                            
                        }
                    }
                    
                    
                    
                }
                
            case .failure(let error):
                if let fail = onFailure {
                    
                    
                    fail(error.localizedDescription)
                     print("sdsdjskdkjsdjksjkdjskdkjskdj error");
                }
            }
        })
    }
   
    func mapView(_ map: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
       
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        //manager.stopUpdatingLocation()
        
   
        
      
        if let pin = annotation as? ColorPointAnnotation {
            
            let identifier = "pinAnnotation"
            
            if let view = map.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKAnnotationView {
                var subtitleView = UILabel()
                //view.pinTintColor = pin.color ?? .red
                
                
                subtitleView.font = subtitleView.font.withSize(10)
                subtitleView.numberOfLines = 11
                subtitleView.text = pin.info
                view.detailCalloutAccessoryView = subtitleView
                view.canShowCallout = true
                view.image = pin.imagen
                
                //  if(view != nil)
                //  {
                return view
                //  }
                
                
                
            } else {
                
                let view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                var subtitleView = UILabel()
                subtitleView.font = subtitleView.font.withSize(10)
                subtitleView.numberOfLines = 11
                subtitleView.text = pin.info
                view.detailCalloutAccessoryView = subtitleView
                view.canShowCallout = true
                
              //  view.pinTintColor = pin.color ?? .red
                view.image = pin.imagen
                return view
            }
        }
        
        return nil
       
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
       
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        
       if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor.blue
            renderer.lineWidth = 2
            return renderer
        }
        
        return MKOverlayRenderer()
    }
  
    fileprivate func parsePositionData(_ data: [[String : AnyObject]]) {
        
        var positions = [Position]()
        
        for p in data {
            let pp = Position()
            pp.setValuesForKeys(p)
            positions.append(pp)
            
       
           
        }
        
        // tell everyone that the positions have been updated
        // decirle a todos que las posiciones han sido actualizadas
        NotificationCenter.default.post(name: Notification.Name(rawValue: Definitions.PositionUpdateNotificationName), object: nil)
    }
    
    
    
  
    
    func loginStatusChanged() {
        
        if User.sharedInstance.isAuthenticated {
            
            //            if shouldCenterOnAppear {
            //                let centerCoordinates = User.sharedInstance.mapCenter
            //                assert(CLLocationCoordinate2DIsValid(centerCoordinates), "Map center coordinates aren't valid")
            //                self.mapView?.setCenter(centerCoordinates, animated: true)
            //
            //                shouldCenterOnAppear = false
            //            }
            
            WebService.sharedInstance.fetchDevices(onSuccess: { (newDevices) in
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                self.devices = newDevices
                
                // if devices are added/removed from the server while user is logged-in, the
                // positions will be added/removed from the map here
              //  self.refreshPositions2()
                self.mapView?.showAnnotations((self.mapView?.annotations)!, animated: false)
                
            })
            
        } else {
           // performSegue(withIdentifier: "ShowLogin", sender: self)
        }
        
        //performSegue(withIdentifier: "ShowLogin", sender: self)
        
        
    }
    
    // MARK: handle the tap of a map pin info button
  

    
    func didTapMapPinDisclosureButton(_ sender: UIButton) {
        if selectedAnnotation != nil {
            performSegue(withIdentifier: "ShowDeviceInfo", sender: self)
        }
    }
    
   
}


