//
//  ColorPointAnnotation.swift
//  com.igpsmobile
//
//  Created by Jonathan Leon.
//  CLASE MAGICA QUE PERMITE ADHERIR CUALQUIER TIPO  DE DATO A UN WAYPOINT * 
//

import Foundation
import MapKit

class ColorPointAnnotation: MKPointAnnotation {
    var positionId: NSNumber?
    var deviceId: NSNumber?
    var color: UIColor!
    var imagen: UIImage!
    var info: String!
}
