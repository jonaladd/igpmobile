/* Code developed by  Jonathan León  */

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}
//let TCDefaultsServerKey = "DefaultsServerKey"
let TCDefaultsAccountKey = "DefaultsAccountKey"
let TCDefaultsEmailKey = "DefaultsEmailKey"
let TCDefaultsPasswordKey = "DefaultsPasswordKey"
let TCDefaultsAlertKey = "DefaultsAlertKey"
let TCDefaultsGpsKey = "DefaultsGpsKey"
class LoginViewController: UIViewController, UITextFieldDelegate {
    
    //   @IBOutlet var serverField: UITextField!
    @IBOutlet var accountField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    var iconClick : Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        iconClick = true
        //iconAction(sender: iconClick as AnyObject)
        // user can't do anything until they're logged-in
        navigationItem.setHidesBackButton(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {

        let d = UserDefaults.standard
        //       if let s = d.string(forKey: TCDefaultsServerKey) {
        //           self.serverField.text = s
        //       }
        if let e = d.string(forKey: TCDefaultsEmailKey) {
            self.emailField.text = e
            
        }
        if let e = d.string(forKey: TCDefaultsAccountKey) {
            self.accountField.text = e
        }
        if let e = d.string(forKey: TCDefaultsPasswordKey) {
            self.passwordField.text = e
           
        }
        
        if
            //self.serverField.text?.characters.count > 0 &&
            self.accountField.text?.characters.count > 0 &&
            self.emailField.text?.characters.count > 0 {
            self.passwordField.becomeFirstResponder()
            self.accountField.isHidden = true
            self.passwordField.isHidden = true
            self.emailField.isHidden = true
            self.loginButtonPressed()
        }else{
            self.accountField.isHidden = false
            self.passwordField.isHidden = false
            self.emailField.isHidden = false
        }
        
        accountField!.becomeFirstResponder()
    }
    @IBAction func iconAction(sender: AnyObject) {
        if(iconClick == true) {
            self.passwordField!.isSecureTextEntry = false
            iconClick = false
        } else {
            self.passwordField!.isSecureTextEntry = true
            iconClick = true
        }
        
    }
    @IBAction func loginButtonPressed() {
        
        MBProgressHUD.showAdded(to: view, animated: true)
        
        //      WebService.sharedInstance.authenticate(serverField!.text!, account: accountField!.text!, email: emailField!.text!, password: passwordField!.text!, onFailure: { errorString in
        
        WebService.sharedInstance.authenticate(account: accountField!.text!, email: emailField!.text!, password: passwordField!.text!, onFailure: { errorString in
            DispatchQueue.main.async(execute: {
                MBProgressHUD.hide(for: self.view, animated: true)
                let ac = UIAlertController(title: "No Hay conexion con el servidor", message: errorString, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                ac.addAction(okAction)
                self.present(ac, animated: true, completion: nil)
            })
            
        }, onSuccess: { (user) in
            DispatchQueue.main.async(execute: {
                
                //  preferencia compartidas del usuario version ios
                let d = UserDefaults.standard
                // d.setValue(self.serverField!.text!, forKey: TCDefaultsServerKey)
                d.setValue(self.passwordField!.text!, forKey: TCDefaultsPasswordKey)
                d.setValue(self.accountField!.text!, forKey: TCDefaultsAccountKey)
                d.setValue(self.emailField!.text!, forKey: TCDefaultsEmailKey)
                // d.setValue("no", forKey: TCDefaultsAlertKey)
                d.synchronize()
                MBProgressHUD.hide(for: self.view, animated: true)
                self.dismiss(animated: true, completion: nil)
            })
        }
        )
    }
    
    // move between text fields when return button pressed, and login
    // when you press return on the password field
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //       if textField == serverField {
        //           emailField.becomeFirstResponder()
        //       } else if textField == emailField {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        } else {
            passwordField.resignFirstResponder()
            loginButtonPressed()
        }
        return true
    }
    
}
